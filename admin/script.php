<?php

/**
 * @version		$Id: script.php 63 2011-04-27 01:35:59Z bfoecke $
 * @package		Themensammlung
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Benjamin F�cke. All rights reserved.
 * @author		Benjamin F�cke
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Script file of Themensammlung component
 */
class com_themensammlungInstallerScript
{
	protected $old_version;

	/**
	 * method to install the component
	 *
	 * @param	JInstallerComponent	$parent	The class calling this method
	 *
	 * @return void
	 */
	function install($parent) 
	{
	}

	/**
	 * method to uninstall the component
	 *
	 * @param	JInstallerComponent	$parent	The class calling this method
	 *
	 * @return void
	 */
	function uninstall($parent) 
	{
		// Get the DB object
		$db = JFactory::getDbo();

		// Delete all categories
		$query = $db->getQuery(true);
		$query->delete();
		$query->from('#__categories');
		$query->where('extension=' . $db->quote('com_themensammlung'));
		$db->setQuery($query);
		$db->query();
		if ($db->getErrorNum()) 
		{
			echo JText::sprintf('JLIB_DATABASE_ERROR_FUNCTION_FAILED', $db->getErrorNum(), $db->getErrorMsg());
			return;
		}
		$table = JTable::getInstance('Category');
		$table->rebuild();
	}

	/**
	 * method to update the component
	 *
	 * @param	JInstallerComponent	$parent	The class calling this method
	 *
	 * @return void
	 */
	function update($parent) 
	{
		// Get the new version
		$new_version = $parent->getParent()->getManifest()->version;                
		if ($this->old_version && $this->old_version == '0.0.1' && version_compare($new_version, '0.0.1') > 0) 
		{
			// Get the DB object
			$db = JFactory::getDbo();

			// Get all entry tables
			$query = 'SHOW TABLES LIKE ' . $db->quote($db->getPrefix() . 'themensammlung_entry_%');
			$db->setQuery($query);
			$tables = $db->loadColumn();

			// Check for a database error.
			if ($db->getErrorNum()) 
			{
				echo JText::sprintf('JLIB_DATABASE_ERROR_FUNCTION_FAILED', $db->getErrorNum(), $db->getErrorMsg());
				return;
			}

			// Update all entry tables
			if (!empty($tables)) 
			{
				foreach ($tables as $table) 
				{
					$query = 'ALTER TABLE `' . $table . '` DROP INDEX `idx_alternate`, ADD INDEX `idx_alternate` (`alternate`)';
					$db->setQuery($query);
					$db->query();

					// Check for a database error.
					if ($db->getErrorNum()) 
					{
						echo JText::sprintf('JLIB_DATABASE_ERROR_FUNCTION_FAILED', $db->getErrorNum(), $db->getErrorMsg());
						return;
					}
				}
			}
		}
	}

	/**
	 * method to run before an install/update/discover_install method
	 *
	 * @param	JInstallerComponent	$parent	The class calling this method
	 * @param 	string				$type  	The type of change (install, update or discover_install)
	 *
	 * @return void
	 */
	function preflight($type, $parent) 
	{
		if ($type == 'update') 
		{
			// Get the DB object
			$db = JFactory::getDbo();

			// Get a new query
			$query = $db->getQuery(true);
			$query->select('s.version_id');
			$query->from('#__extensions AS e');
			$query->leftJoin('#__schemas AS s ON e.extension_id=s.extension_id');
			$query->where('e.element=' . $db->quote('com_themensammlung'));
			$db->setQuery($query);
			$this->old_version = $db->loadResult();
		}
	}

	/**
	 * method to run after an install/update/discover_install method
	 *
	 * @param	JInstallerComponent	$parent	The class calling this method
	 * @param 	string				$type  	The type of change (install, update or discover_install)
	 *
	 * @return void
	 */
	function postflight($type, $parent) 
	{
		// Redirect to the component main view after installing
                	// Redirect to the component main view after installing
            if(!defined('DS')){
                define('DS',DIRECTORY_SEPARATOR);
                }
            $installer = new JInstaller();
            $installer->install($parent->getParent()->getPath('source').DS.'plugins'.DS.'themensammlungentry');
            $installer->install($parent->getParent()->getPath('source').DS.'plugins'.DS.'themencontact');
            $installer->install($parent->getParent()->getPath('source').DS.'plugins'.DS.'search_themensammlung');
            $installer->install($parent->getParent()->getPath('source').DS.'plugins'.DS.'categoryreplacethemen');
            
            $installer->install($parent->getParent()->getPath('source').DS.'modules'.DS.'mod_joeswordcloud_v3_2_3');
            
            //$parent->getParent()->setRedirectURL('index.php?option=com_coursedatabase');
		
            // Enable plugins
            $db =& JFactory::getDBO();
            $db->setQuery("UPDATE #__extensions SET enabled = 1 WHERE ".
				"element = 'themensammlungentry' ");
            $db->query();
            $db->setQuery("UPDATE #__extensions SET enabled = 1 WHERE ".
                                    "element = 'themencontact' ");
            $db->query();
            $db->setQuery("UPDATE #__extensions SET enabled = 1 WHERE ".
                                    "element = 'themensammlung' ");
            $db->query();
            $db->setQuery("UPDATE #__extensions SET enabled = 1 WHERE ".
                                    "element = 'categoryreplacethemen' ");
            $db->query();
	}
}
