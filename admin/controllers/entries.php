<?php

/**
 * @version		$Id: entries.php 63 2011-04-27 01:35:59Z bfoecke $
 * @package		Themensammlung
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Benjamin F�cke, Inc. All rights reserved.
 * @author		Benjamin F�cke
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controlleradmin library
//jimport('joomla.application.component.controlleradmin');

/**
 * Entries Controller of Themensammlung component
 *
 * @since	0.0.1
 */
class ThemensammlungControllerEntries extends JControllerAdmin
{
        
        /**
	 * Constructor.
	 *
	 * @param   array  $config	An optional associative array of configuration settings.

	 * @return  ThemensammlungControllerEntries
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);

		// Entries default form can come from the entries view.
		// Adjust the redirect view on the value of 'view' in the request.
		if ($this->input->get('view') == 'featured')
		{
			$this->view_list = 'featured';
		}

		$this->registerTask('unfeatured',	'featured');
	}
        
        
	/**
	 * Method to get a model object, loading it if required.
	 *
	 * @param	string	$name	The model name. Optional.
	 * @param	string	$prefix	The class prefix. Optional.
	 * @param	array	$config	Configuration array for model. Optional.
	 *
	 * @return	object	The model.
	 *
	 * @since	0.0.1
	 *
	 * @see		JControllerForm::getModel
	 */
	public function getModel($name = 'Entry', $prefix = 'ThemensammlungModel', $config = array('ignore_request' => true)) 
	{
		return parent::getModel($name, $prefix, $config);
	}
}
