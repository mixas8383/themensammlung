<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of pdf
 *
 * @author tiloziegler
 */
jimport('joomla.application.component.controllerform');

class ThemensammlungControllerPdf extends JControllerForm{
        
    function remove(){
        $json           = JRequest::getVar('json');
                       
        $data = json_decode($json);

        if(!isset($data->id) || empty($data->id) || !filter_var($data->id, FILTER_VALIDATE_INT)){
            $response = array(
                            'status' => 0,
                            'msg' => JText::_('COM_THEMENSAMMLUNG_PDF_DELETE_INVALID_ID')
                    );
            echo json_encode($response);
            return;
        }

        $dbo        = JFactory::getDbo();                                  // JQuery Datenbank-Objekt abrufen
        $dbo->setQuery("DELETE FROM #__themensammlung_entry_pdfs WHERE id = {$data->id}");
        if($dbo->query()){
            $response = array(
                            'status' => 1,
                            'msg' => JText::_('COM_THEMENSAMMLUNG_PDF_DELETE_SUCCESSFUL')
                    );
            echo json_encode($response);
            return;
        }else{
            $response = array(
                            'status' => 0,
                            'msg' => JText::_('COM_THEMENSAMMLUNG_PDF_DELETE_FAILURE')
                    );
            echo json_encode($response);
            return;
        }
        
    }
}
?>
