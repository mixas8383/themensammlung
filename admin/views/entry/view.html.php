<?php

/**
 * @version		$Id: view.html.php 63 2011-04-27 01:35:59Z bfoecke $
 * @package		Themensammlung
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Benjamin F�cke. All rights reserved.
 * @author		Benjamin F�cke
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Entry View of Themensammlung component
 *
 * @since	0.0.1
 */
class ThemensammlungViewEntry extends JViewLegacy
{
        protected $form;

	protected $item;

	protected $state;
        
	/**
	 * Execute and display a layout script.
	 *
	 * @param	string $tpl	The name of the layout file to parse.
	 *
	 * @return	void|JError
	 *
	 * @see		JViewLegacy::display
	 */
	public function display($tpl = null) 
	{
		// Initialiase variables.
		$this->form		= $this->get('Form');
		$this->item		= $this->get('Item');
		$this->state            = $this->get('State');
                $this->canDo            = ThemensammlungHelper::getActions($this->state->get('filter.category_id'));
                
		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
			JError::raiseError(500, implode("<br />", $errors));
			return false;
		}

		// Add toolbar
		$this->addToolbar();

		// Display the layout
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return	void
	 */
	protected function addToolbar() 
	{
		// Load specific css component
		JHtml::_('stylesheet', 'com_themensammlung/administrator/themensammlung.css', array(), true);
                
                JFactory::getApplication()->input->set('hidemainmenu', true);		
		$user = JFactory::getUser();
		$userId = $user->get('id');
		$isNew = ($this->item->id == 0);
		$checkedOut = !($this->item->checked_out == 0 || $this->item->checked_out == $userId);
		$canDo = ThemensammlungHelper::getActions($this->state->get('filter.category_id'), $this->item->id);
		JToolBarHelper::title(JText::_('COM_THEMENSAMMLUNG_ENTRY_' . ($isNew ? 'NEW' : 'EDIT') . '_TITLE'), 'themensammlung-' . ($isNew ? 'new' : 'edit'));

		// Built the actions for new and existing records.
                // For new records, check the create permission.
                if ($isNew && (count($user->getAuthorisedCategories('com_themensammlung', 'core.create')) > 0 || $canDo->get('core.create'))) 
                    {
                            JToolBarHelper::apply('entry.apply', 'JTOOLBAR_APPLY');
                            JToolBarHelper::save('entry.save', 'JTOOLBAR_SAVE');
                            JToolBarHelper::custom('entry.save2new', 'save-new.png', 'save-new_f2.png', 'JTOOLBAR_SAVE_AND_NEW', false);
                            JToolBarHelper::cancel('entry.cancel', 'JTOOLBAR_CANCEL');
                    }
                    
		else
		{
			// Can't save the record if it's checked out.
			if (!$checkedOut) 
			{
				// Since it's an existing record, check the edit permission, or fall back to edit own if the owner.
				if ($canDo->get('core.edit') || ($canDo->get('core.edit.own') && $this->item->created_by == $userId)) 
				{
					JToolBarHelper::apply('entry.apply', 'JTOOLBAR_APPLY');
					JToolBarHelper::save('entry.save', 'JTOOLBAR_SAVE');

					// We can save this record, but check the create permission to see if we can return to make a new one.
					if ($canDo->get('core.create')) 
					{
						JToolBarHelper::custom('entry.save2new', 'save-new.png', 'save-new_f2.png', 'JTOOLBAR_SAVE_AND_NEW', false);
					}
				}
			}

			// If checked out, we can still save
			if ($canDo->get('core.create')) 
			{
				JToolBarHelper::custom('entry.save2copy', 'save-copy.png', 'save-copy_f2.png', 'JTOOLBAR_SAVE_AS_COPY', false);
			}
			JToolBarHelper::cancel('entry.cancel', 'JTOOLBAR_CLOSE');
		}
		JToolBarHelper::help('COM_THEMENSAMMLUNG_HELP_ENTRY_EDIT');
	}
	
        /**
	 * Method to set up the document properties
	 *
	 * @return	void
	 */
	protected function prepareDocument() 
	{
            
	}
        
        protected function  format_bytes($size) {
            $units = array(' B', ' KB', ' MB', ' GB', ' TB');
            for ($i = 0; $size >= 1024 && $i < 4; $i++) $size /= 1024;
            return round($size, 1).$units[$i];
        }
}
