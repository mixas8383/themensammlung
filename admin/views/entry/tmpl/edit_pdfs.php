<?php

/**
 * @version		$Id: edit_params.php 56 2011-04-05 20:20:35Z chdemko $
 * @package		Themensammlung
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Christophe Demko. All rights reserved.
 * @author		Christophe Demko
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
$action_pdf_remove = JURI::base() . 'index.php?option=com_themensammlung&task=pdf.remove&format=json';
?>
<div id="pdftext" ></div>
<fieldset class="adminform">
    <legend><?php echo JText::_('PDF-Datei hochladen:'); ?></legend>
    <p>Achtung: Die Dateien werden erst beim Speichern der Veranstaltung hochgeladen!</p>
</fieldset>
<div class="row-fluid" id="uploadform">
    <div class="span6" id="pdf_upload_list">
        <div class="control-group">
            <?php echo $this->form->getLabel('pdfupload'); ?>
            <div class="controls"><?php echo $this->form->getInput('pdfupload'); ?></div>
        </div>
    </div>
</div>
<p><img style="float:none; margin: 0; cursor: pointer;" src="../media/com_themensammlung/images/administrator/icon-16-themensammlung-entry-new.png" alt="<?=JText::_('COM_THEMENSAMMLUNG_PDF_ADD_INPUT')?>" title="<?=JText::_('COM_THEMENSAMMLUNG_PDF_ADD_INPUT')?>" onclick="javascript:Joomla.addPdfUploadForm()" /><?php echo " Noch eine weitere Datei hochladen..."; ?></p>
<fieldset class="adminform">
    <legend>Hochgeladene PDFs:</legend>
</fieldset>
<div class="row-fluid">
    <div class="span6" id="pdflist">
        <?php foreach($this->item->pdfs as $pdf): ?>
            <div id="pdf_<?=$pdf->id ?>"><?=$this->format_bytes($pdf->filesize) ?><img style="float: none; margin: 0 5px !important; padding: 0px; cursor: pointer;" onclick="javascript:Joomla.removepdf(<?=$pdf->id ?>, '<?=$action_pdf_remove ?>')" src="../media/com_themensammlung/images/administrator/icon-12-themensammlung-pdf-delete.png" alt="COM_THEMENSAMMLUNG_PDF_DELETE" title="<?=JText::_('COM_THEMENSAMMLUNG_PDF_DELETE')?>" /><a href="<?=$pdf->id ?>" title="<?=$pdf->filename ?>"><?=$pdf->filename ?></a></div>
        <?php endforeach; ?>
    </div>
</div>