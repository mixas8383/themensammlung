<?php

/**
 * @version		$Id: default_body.php 63 2011-04-27 01:35:59Z bfoecke $
 * @package		Themensammlung
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Benjamin F�cke. All rights reserved.
 * @author		Benjamin F�cke
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
$user		= JFactory::getUser();
$userId		= $user->get('id');
$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));
$archived	= $this->state->get('filter.published') == 2 ? true : false;
$trashed	= $this->state->get('filter.published') == -2 ? true : false;
$saveOrder	= $listOrder == 'ordering';

if ($saveOrder)
{
	$saveOrderingUrl = 'index.php?option=com_themensammlung&task=entries.saveOrderAjax&tmpl=component';
	JHtml::_('sortablelist.sortable', 'entryList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}
$n			= count($this->items);
?>
<?php foreach($this->items as $i => $item):
		$ordering	= ($listOrder == 'ordering');
		$canCreate	= $user->authorise('core.create',		'com_themensammlung.category.'.$item->catid);
		$canEdit	= $user->authorise('core.edit',			'com_themensammlung.entry.'.$item->id);
		$canCheckin	= $user->authorise('core.manage',		'com_checkin') || $item->checked_out == $userId || $item->checked_out == 0;
		$canEditOwn	= $user->authorise('core.edit.own',		'com_themensammlung.entry.'.$item->id) && $item->created_by == $userId;
		$canChange	= $user->authorise('core.edit.state',	'com_themensammlung.entry.'.$item->id) && $canCheckin;
?>
	<tr class="row<?php echo $i % 2; ?>" sortable-group-id="<?php echo $item->catid; ?>">
						<td class="order nowrap center hidden-phone">
                        <?php
                        $iconClass = '';
                        if (!$canChange)
                        {
                                $iconClass = ' inactive';
                        }
                        elseif (!$saveOrder)
                        {
                                $iconClass = ' inactive tip-top hasTooltip" title="' . JHtml::tooltipText('JORDERINGDISABLED');
                        }
                        ?>
                        <span class="sortable-handler<?php echo $iconClass ?>">
                                <i class="icon-menu"></i>
                        </span>
                        <?php if ($canChange && $saveOrder) : ?>
                                <input type="text" style="display:none" name="order[]" size="5" value="<?php echo $item->ordering; ?>" class="width-20 text-area-order " />
                        <?php endif; ?>
                </td>
                <td class="center">
                        <?php echo JHtml::_('grid.id', $i, $item->id); ?>
                </td>
                <td class="center">
                        <div class="btn-group">
                                <?php echo JHtml::_('jgrid.published', $item->published, $i, 'entries.', $canChange, 'cb', $item->publish_up, $item->publish_down); ?>
                                
                                <?php
                                // Create dropdown items
                                $action = $archived ? 'unarchive' : 'archive';
                                JHtml::_('actionsdropdown.' . $action, 'cb' . $i, 'entries');

                                $action = $trashed ? 'untrash' : 'trash';
                                JHtml::_('actionsdropdown.' . $action, 'cb' . $i, 'entries');

                                // Render dropdown list
                                echo JHtml::_('actionsdropdown.render', $this->escape($item->title));
                                ?>
                        </div>
                </td>
		<td class="has-context">
                        <div class="pull-left break-word">
                                <?php if ($item->checked_out) : ?>
                                        <?php echo JHtml::_('jgrid.checkedout', $i, $item->editor, $item->checked_out_time, 'entries.', $canCheckin); ?>
                                <?php endif; ?>
                                <?php if ($item->language == '*'):?>
                                        <?php $language = JText::alt('JALL', 'language'); ?>
                                <?php else:?>
                                        <?php $language = $item->language_title ? $this->escape($item->language_title) : JText::_('JUNDEFINED'); ?>
                                <?php endif;?>
                                <?php if ($canEdit || $canEditOwn) : ?>
                                        <a class="hasTooltip" href="<?php echo JRoute::_('index.php?option=com_themensammlung&task=entry.edit&id=' . $item->id); ?>" title="<?php echo JText::_('JACTION_EDIT'); ?>">
                                                <?php echo $this->escape($item->title); ?></a>
                                <?php else : ?>
                                        <span title="<?php echo JText::sprintf('JFIELD_ALIAS_LABEL', $this->escape($item->alias)); ?>"><?php echo $this->escape($item->title); ?></span>
                                <?php endif; ?>
                                <?php if ($item->subtitle) : ?>
                                    <p class="small break-word">
                                        <?php echo $this->escape($item->subtitle); ?>
                                    </p>
                                <?php endif; ?>
                                <span class="small break-word">
                                        <?php echo JText::sprintf('JGLOBAL_LIST_ALIAS', $this->escape($item->alias)); ?>
                                </span>
                        </div>
                </td>
                <td class="small hidden-phone">
                        <?php echo $this->escape($item->access_level); ?>
                </td>
                <td class="center" nowrap>
			<?php echo $this->escape($item->category_title); ?>
		</td>
                <td class="small hidden-phone">
                        <?php if ($item->author_alias) : ?>
                                <a class="hasTooltip" href="<?php echo JRoute::_('index.php?option=com_contact&task=contact.edit&id=' . (int) $item->author); ?>" title="<?php echo JText::_('JAUTHOR'); ?>">
                                <?php echo $this->escape($item->author_name); ?></a>
                                <p class="smallsub"> <?php echo JText::sprintf('JGLOBAL_LIST_ALIAS', $this->escape($item->author_alias)); ?></p>
                        <?php else : ?>
                                <a class="hasTooltip" href="<?php echo JRoute::_('index.php?option=com_contact&task=contact.edit&id=' . (int) $item->author); ?>" title="<?php echo JText::_('JAUTHOR'); ?>">
                                <?php echo $this->escape($item->author_name); ?></a>
                        <?php endif; ?>
                </td>
                <td class="small hidden-phone">
                        <?php if ($item->language == '*'):?>
                                <?php echo JText::alt('JALL', 'language'); ?>
                        <?php else:?>
                                <?php echo $item->language_title ? $this->escape($item->language_title) : JText::_('JUNDEFINED'); ?>
                        <?php endif;?>
                </td>
                <td class="nowrap small hidden-phone">
                        <?php echo JHtml::_('date', $item->created, JText::_('DATE_FORMAT_LC4')); ?>
                </td>
                <td class="hidden-phone">
                        <?php echo (int) $item->hits; ?>
                </td>
                <td class="hidden-phone">
                        <?php echo (int) $item->id; ?>
                </td>
	</tr>
<?php endforeach; ?>

