<?php

/**
 * @version		$Id: default.php 63 2011-04-27 01:35:59Z bfoecke $
 * @package		Themensammlung
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Benjamin F�cke. All rights reserved.
 * @author		Benjamin F�cke
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$app		= JFactory::getApplication();
$user		= JFactory::getUser();
$userId		= $user->get('id');
$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));
$archived	= $this->state->get('filter.published') == 2 ? true : false;
$trashed	= $this->state->get('filter.published') == -2 ? true : false;
$saveOrder	= $listOrder == 'a.ordering';
$columns   = 10;

if ($saveOrder)
{
	$saveOrderingUrl = 'index.php?option=com_themensammlung&task=entries.saveOrderAjax&tmpl=component';
	JHtml::_('sortablelist.sortable', 'entryList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}

$assoc = JLanguageAssociations::isEnabled();
?>

<form action="<?php echo JRoute::_('index.php?option=com_themensammlung&view=entries'); ?>" method="post" name="adminForm" id="adminForm">
<?php if (!empty( $this->sidebar)) : ?>
        <div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif;?>
                <?php
		// Search tools bar
		echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
                ?>
            
                <?php if (empty($this->items)) : ?>
			<div class="alert alert-no-items">
				<?php echo JText::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
			</div>
		<?php else : ?>
                    <table class="table table-striped" id="entryList">
                            <thead><?php echo $this->loadTemplate('head');?></thead>
                            <tbody><?php echo $this->loadTemplate('body');?></tbody>
                    </table>            
                    <?php // Load the batch processing form. ?>
                    <?php if ($user->authorise('core.create', 'com_themensammlung')
                            && $user->authorise('core.edit', 'com_themensammlung')
                            && $user->authorise('core.edit.state', 'com_themensammlung')) : ?>
                            <?php echo JHtml::_(
                                    'bootstrap.renderModal',
                                    'collapseModal',
                                    array(
                                            'title' => JText::_('COM_CONTENT_BATCH_OPTIONS'),
                                            'footer' => $this->loadTemplate('batch_footer')
                                    ),
                                    $this->loadTemplate('batch_body')
                            ); ?>
                    <?php endif; ?>
                    <?php endif; ?>
                    <?php echo $this->pagination->getListFooter(); ?>
                    <input type="hidden" name="task" value="" />
                    <input type="hidden" name="boxchecked" value="0" />
                    <?php echo JHtml::_('form.token'); ?>
                
            </div>
</form>