<?php

/**
 * @version		$Id: view.html.php 63 2011-04-27 01:35:59Z bfoecke $
 * @package		Themensammlung
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Benjamin F�cke. All rights reserved.
 * @author		Benjamin F�cke
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

/**
 * Entries View of Themensammlung component
 *
 * @since	0.0.1
 */
class ThemensammlungViewEntries extends JViewLegacy
{
        protected $items;

	protected $pagination;

	protected $state;
	/**
	 * Execute and display a layout script.
	 *
	 * @param	string $tpl	The name of the layout file to parse.
	 *
	 * @return 	void|JError
	 *
	 * @see		JViewLegacy::display
	 */
	public function display($tpl = null) 
	{
                if ($this->getLayout() !== 'modal')
		{
			ThemensammlungHelper::addSubmenu('entries');
		}
                
		// Get data from the model
		$this->items		= $this->get('Items');
		$this->pagination	= $this->get('Pagination');
		$this->state		= $this->get('State');
                $this->authors          = $this->get('Authors');
		$this->filterForm       = $this->get('FilterForm');
		$this->activeFilters    = $this->get('ActiveFilters');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		
                // Levels filter.
		$options	= array();
		$options[]	= JHtml::_('select.option', '1', JText::_('J1'));
		$options[]	= JHtml::_('select.option', '2', JText::_('J2'));
		$options[]	= JHtml::_('select.option', '3', JText::_('J3'));
		$options[]	= JHtml::_('select.option', '4', JText::_('J4'));
		$options[]	= JHtml::_('select.option', '5', JText::_('J5'));
		$options[]	= JHtml::_('select.option', '6', JText::_('J6'));
		$options[]	= JHtml::_('select.option', '7', JText::_('J7'));
		$options[]	= JHtml::_('select.option', '8', JText::_('J8'));
		$options[]	= JHtml::_('select.option', '9', JText::_('J9'));
		$options[]	= JHtml::_('select.option', '10', JText::_('J10'));

		$this->f_levels = $options;
                
		// Set the toolbar
		$this->addToolBar();
                $this->sidebar = JHtmlSidebar::render();
                
		// Display the template
		parent::display($tpl);
	}

	/**
	 * Setting the toolbar
	 *
	 * @return 	void
	 */
	protected function addToolBar() 
	{
		// Load specific css component
		JHtml::_('stylesheet', 'com_themensammlung/administrator/themensammlung.css', array(), true);
		          
                $canDo = JHelperContent::getActions('com_themensammlung', 'category', $this->state->get('filter.category_id'));
		//$canDo = ThemensammlungHelper::getActions($this->state->get('filter.category_id'));
		$user = JFactory::getUser();
                
                // Get the toolbar object instance
		$bar = JToolBar::getInstance('toolbar');
                
		JToolBarHelper::title(JText::_('COM_THEMENSAMMLUNG_ENTRIES_TITLE'), 'themensammlung');
                
		if ($canDo->get('core.create') || (count($user->getAuthorisedCategories('com_themensammlung', 'core.create'))) > 0) 
		{
			JToolBarHelper::addNew('entry.add', 'JTOOLBAR_NEW');
		}
                
		if (($canDo->get('core.edit')) || ($canDo->get('core.edit.own'))) 
		{
			JToolBarHelper::editList('entry.edit', 'JTOOLBAR_EDIT');
		}
                
		if ($canDo->get('core.edit.state')) 
		{
			JToolBarHelper::custom('entries.publish', 'publish.png', 'publish_f2.png', 'JTOOLBAR_PUBLISH', true);
			JToolBarHelper::custom('entries.unpublish', 'unpublish.png', 'unpublish_f2.png', 'JTOOLBAR_UNPUBLISH', true);
			JToolBarHelper::archiveList('entries.archive', 'JTOOLBAR_ARCHIVE');
			JToolBarHelper::custom('entries.checkin', 'checkin.png', 'checkin_f2.png', 'JTOOLBAR_CHECKIN', true);
		}
		// Add a batch button - wird nicht ben�tigt
		if ($user->authorise('core.create', 'com_themensammlung')
			&& $user->authorise('core.edit', 'com_themensammlung')
			&& $user->authorise('core.edit.state', 'com_themensammlung'))
		{

			$title = JText::_('JTOOLBAR_BATCH');

			// Instantiate a new JLayoutFile instance and render the batch button
			$layout = new JLayoutFile('joomla.toolbar.batch');

			$dhtml = $layout->render(array('title' => $title));
			$bar->appendButton('Custom', $dhtml, 'batch');
		}
                
		if ($this->state->get('filter.published') == - 2 && $canDo->get('core.delete')) 
		{
			JToolBarHelper::deleteList('', 'entries.delete', 'JTOOLBAR_EMPTY_TRASH');
		}
		else if ($canDo->get('core.edit.state')) 
		{
			JToolBarHelper::trash('entries.trash', 'JTOOLBAR_TRASH');
		}
                                
		if ($user->authorise('core.admin', 'com_themensammlung') || $user->authorise('core.options', 'com_themensammlung'))
		{
			JToolbarHelper::preferences('com_themensammlung');
		}

		JToolbarHelper::help('JHELP_CONTENT_ARTICLE_MANAGER');
                
	}

        
        /**
	 * Returns an array of fields the table can be sorted by
	 *
	 * @return  array  Array containing the field name to sort by as the key and display text as value
	 *
	 * @since   3.0
	 */
	protected function getSortFields()
	{
		return array(
			'ordering' => JText::_('JGRID_HEADING_ORDERING'),
                        'title' => JText::_('JGLOBAL_TITLE'),
			'published' => JText::_('JSTATUS'),
			'region' => JText::_('Gruppe'),
			'access_level' => JText::_('JGRID_HEADING_ACCESS'),
			'author' => JText::_('JAUTHOR'),
                        'created' => JText::_('JDATE'),
			'language' => JText::_('JGRID_HEADING_LANGUAGE'),
                        'hits'     => JText::_('JHITS'),
			'id' => JText::_('JGRID_HEADING_ID')
		);
	}
}
