<?php

/**
 * @version		$Id: entries.php 63 2011-04-27 01:35:59Z bfoecke $
 * @package		Themensammlung
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Benjamin F�cke. All rights reserved.
 * @author		Benjamin F�cke
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import the Joomla modellist library
//jimport('joomla.application.component.modellist');

// import the joomla language library
//jimport('joomla.language.helper');

// import component helper
//JLoader::register('ThemensammlungHelper', JPATH_ADMINISTRATOR . '/components/com_themensammlung/helpers/themensammlung.php');

/**
 * Entries Model of Themensammlung component
 *
 * @since	0.0.1
 */
class ThemensammlungModelEntries extends JModelList
{
	/**
	 * @var		array	 The authorized ordering fields.
	 *
	 * @since	0.0.1
	 *
	 * @see		JModelList::$filter_fields
	 */
	//protected $filter_fields = array('title', 'published', 'region', 'category_title', 'ordering', 'access_level', 'created_by', 'created', 'modified', 'hits', 'language', 'id');

	/**
	 * Constructor.
	 *
	 * @param	array	$config An optional associative array of configuration settings.
	 *
	 * @since	0.0.1
	 *
	 * @see		JModelList::__construct
	 */
        public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.id',
				'title', 'a.title',
				'alias', 'a.alias',
				'checked_out', 'a.checked_out',
				'checked_out_time', 'a.checked_out_time',
				'catid', 'a.catid', 'category_title',
				'state', 'a.state',
				'access', 'a.access', 'access_level',
				'author', 'a.author',
				'author_alias', 'a.author_alias',
				'created', 'a.created',
				'created_by', 'a.created_by',
				'created_by_alias', 'a.created_by_alias',
				'ordering', 'a.ordering',
				/*'featured', 'a.featured',*/
				'language', 'a.language',
				'hits', 'a.hits',
				'publish_up', 'a.publish_up',
				'publish_down', 'a.publish_down',
				'published', 'a.published',
				'author_id',
				'category_id',
				'level',
				'tag'
			);

			if (JLanguageAssociations::isEnabled())
			{
				$config['filter_fields'][] = 'association';
			}
		}

		parent::__construct($config);
	}
        
	/*public function __construct($config = array()) 
	{
		parent::__construct($config);

		// Get all lan,guage tags
		$tags = JArrayHelper::getColumn(JLanguageHelper::getLanguages(), 'lang_code');

		// Create tables for tags
		ThemensammlungHelper::createTitleTables($tags);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * @param	string	$ordering	An optional ordering field.
	 * @param	string	$direction	An optional direction (asc|desc).
	 *
	 * @return void
	 *
	 * @note	List of internal states
	 *			- filter.search			title to search for
	 *			- filter.access			array of access levels for filtering
	 *			- filter.published		array of states for filtering
	 *			- filter.category_id	array of catid for filtering
	 *			- filter.language		array of language code for filtering
	 *			- list.limit			number of items retrieved for the collection (defined in JModelList)
	 *			- list.start			starting index of items for the collection (defined in JModelList)
	 *			- list.ordering			field for ordering the collection
	 *			- list.direction		direction of ordering (asc or desc)
	 *
	 * @since	0.0.1
	 *
	 * @see		JModelList::populateState
	 */
	protected function populateState($ordering = null, $direction = null) 
	{
                $app = JFactory::getApplication();

		// Adjust the context to support modal layouts.
		if ($layout = $app->input->get('layout'))
		{
			$this->context .= '.' . $layout;
		}
                
		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$access = $this->getUserStateFromRequest($this->context . '.filter.access', 'filter_access');
		$this->setState('filter.access', $access);

		$authorId = $app->getUserStateFromRequest($this->context . '.filter.author_id', 'filter_author_id');
		$this->setState('filter.author_id', $authorId);

		$published = $this->getUserStateFromRequest($this->context . '.filter.published', 'filter_published', '');
		$this->setState('filter.published', $published);

		$categoryId = $this->getUserStateFromRequest($this->context . '.filter.category_id', 'filter_category_id');
		$this->setState('filter.category_id', $categoryId);

		$level = $this->getUserStateFromRequest($this->context . '.filter.level', 'filter_level');
		$this->setState('filter.level', $level);

		$language = $this->getUserStateFromRequest($this->context . '.filter.language', 'filter_language', '');
		$this->setState('filter.language', $language);

		$tag = $this->getUserStateFromRequest($this->context . '.filter.tag', 'filter_tag', '');
		$this->setState('filter.tag', $tag);

        $publish_down = $this->getUserStateFromRequest($this->context . '.filter.publish_down', 'filter_publish_down', '');
        $this->setState('filter.publish_down', $publish_down);

                // List state information.
		parent::populateState('a.id', 'desc');

		// force a language
		$forcedLanguage = $app->input->get('forcedLanguage');
                
		if (!empty($forcedLanguage))
		{
			$this->setState('filter.language', $forcedLanguage);
			$this->setState('filter.forcedLanguage', $forcedLanguage);
		}

		$tag = $this->getUserStateFromRequest($this->context . '.filter.tag', 'filter_tag', '');
		$this->setState('filter.tag', $tag);

		// List state information.
		parent::populateState('a.id', 'desc');
                
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return  string  A store id.
	 *
	 * @since   1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . serialize($this->getState('filter.search'));
		$id .= ':' . serialize($this->getState('filter.access'));
		$id .= ':' . serialize($this->getState('filter.published'));
		$id .= ':' . serialize($this->getState('filter.category_id'));
		$id .= ':' . serialize($this->getState('filter.author_id'));
		$id .= ':' . serialize($this->getState('filter.language'));
        $id .= ':' . serialize($this->getState('filter.publish_down'));

		return parent::getStoreId($id);
	}

        /**
	 * Method to get a JDatabaseQuery object for retrieving the data set from a database.
	 *
	 * @return	object	A JDatabaseQuery object to retrieve the data set.
	 *
	 * @since	0.0.1
	 *
	 * @see		JModelList::getListQuery
	 */
	protected function getListQuery() 
	{
		// Create a new query object.
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$user = JFactory::getUser();
		$app = JFactory::getApplication();

		// Select some fields
		$query->select(
			$this->getState(
				'list.select',
				'a.id,a.published,a.title,a.subtitle,a.text,a.alias,a.catid,a.created,a.author, a.created_by,a.author_alias, a.created_by_alias,a.modified,a.checked_out,a.checked_out_time,'.
			'a.hits,a.language,a.ordering,a.introduction,a.text2,a.publish_up,a.publish_down'
			)
		);

		// From the themensammlung_entry table
		$query->from('#__themensammlung_entry as a');

        //$current_date = date( "Y-m-j H:i:00", strtotime( 'now' ) );
        //$query->where( '(' . $db->quoteName('a.publish_down') . ' >= ' . $db->quote( $current_date ) . ')'
        //    . ' OR (' .  $db->quoteName('a.publish_down') . ' = ' . $db->quote( '0000-00-00 00:00:00' ) . ')' );

                // Join over the alternative categories.
		$query->select('acat.alternativecatid')
                        ->join('LEFT', '#__themensammlung_entry_categories AS acat ON acat.eid = a.id');    
                
                // Join over the language
		$query->select('l.title AS language_title')
			->join('LEFT', $db->quoteName('#__languages') . ' AS l ON l.lang_code = a.language');
                
                // Join over the view levels.
                $query->select('ag.title AS access_level');
                    $query->join('LEFT', '#__viewlevels AS ag ON ag.id = a.access');
		
                // Join over the categories.
		$query->select('c.title AS category_title')
			->join('LEFT', '#__categories AS c ON c.id = a.catid');
                
                // Join over the users for the author.
		$query->select('ua.name AS author_name');
                //$query->select('ua.email AS author_email');
                $query->join('LEFT', '#__contact_details AS ua ON ua.id = a.author');

                // Join over the users for the checked out user.
		$query->select('uc.name AS editor')
			->join('LEFT', '#__users AS uc ON uc.id=a.checked_out');

		            		
                // Join over the associations.
		if (JLanguageAssociations::isEnabled())
		{
			$query->select('COUNT(asso2.id)>1 as association')
				->join('LEFT', '#__associations AS asso ON asso.id = a.id AND asso.context=' . $db->quote('com_themensammlung.item'))
				->join('LEFT', '#__associations AS asso2 ON asso2.key = asso.key')
				->group('a.id');
		}

		// Filter by access level.
		if ($access = $this->getState('filter.access'))
		{
			$query->where('a.access = ' . (int) $access);
		}

        if ($access = $this->getState('filter.publish_down'))
        {
            $current_date = date( "Y-m-j H:i:00", strtotime( 'now' ) );
            $query->where( '(' . $db->quoteName('a.publish_down') . ' >= ' . $db->quote( $current_date ) . ')'
                . ' OR (' .  $db->quoteName('a.publish_down') . ' = ' . $db->quote( '0000-00-00 00:00:00' ) . ')' );
        }

		// Implement View Level Access
		if (!$user->authorise('core.admin'))
		{
			$groups = implode(',', $user->getAuthorisedViewLevels());
			$query->where('a.access IN (' . $groups . ')');
		}

		// Filter by published state
		$published = $this->getState('filter.published');

		if (is_numeric($published))
		{
			$query->where('a.published = ' . (int) $published);
		}
		else if ($published=="*")
		{
			$query->where('a.published IN (-2,0,1,2)');
		}
                else
		{
			$query->where('(a.published = 1 OR a.published = 0)');
		}
                
                // Filter by a single or group of categories.
		$baselevel = 1;
		$categoryId = $this->getState('filter.category_id');

		if (is_numeric($categoryId))
		{
			$cat_tbl = JTable::getInstance('Category', 'JTable');
			$cat_tbl->load($categoryId);
			$rgt = $cat_tbl->rgt;
			$lft = $cat_tbl->lft;
			$baselevel = (int) $cat_tbl->level;
			$query->where('c.lft >= ' . (int) $lft)
				->where('c.rgt <= ' . (int) $rgt);
		}
		elseif (is_array($categoryId))
		{
			JArrayHelper::toInteger($categoryId);
			$categoryId = implode(',', $categoryId);
			$query->where('a.catid IN (' . $categoryId . ')');
		}

                // Filter on the level.
		if ($level = $this->getState('filter.level'))
		{
			$query->where('c.level <= ' . ((int) $level + (int) $baselevel - 1));
		}

		// Filter by author
		/*$authorId = $this->getState('filter.author_id');

		if (is_numeric($authorId))
		{
			$type = $this->getState('filter.author_id.include', true) ? '= ' : '<>';
			$query->where('a.author ' . $type . (int) $authorId);
		}*/

		// Filter by search in title.
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			elseif (stripos($search, 'author:') === 0)
			{
				$search = $db->quote('%' . $db->escape(substr($search, 7), true) . '%');
				$query->where('(ua.name LIKE ' . $search . ' OR ua.username LIKE ' . $search . ')');
			}
			else
			{
				$search = $db->quote('%' . $db->escape($search, true) . '%');
				$query->where('(a.title LIKE ' . $search . ' OR a.alias LIKE ' . $search . ')');
			}
		}

		// Filter on the language.
		if (is_array($language = $this->getState('filter.language'))) 
                { 
                        $query->where('a.language IN ("*","' . implode(',', $language) . '")'); 
                } 
		else if ($language = $this->getState('filter.language'))
		{
			$query->where('a.language = ' . $db->quote($language));
		}

		// Filter by a single tag.
		$tagId = $this->getState('filter.tag');

		if (is_numeric($tagId))
		{
			$query->where($db->quoteName('tagmap.tag_id') . ' = ' . (int) $tagId)
				->join(
					'LEFT', $db->quoteName('#__themensammlungitem_tag_map', 'tagmap')
					. ' ON ' . $db->quoteName('tagmap.themensammlung_item_id') . ' = ' . $db->quoteName('a.id')
					. ' AND ' . $db->quoteName('tagmap.type_alias') . ' = ' . $db->quote('com_themensammlung.entry')
				);
		}

		// Add the list ordering clause.
		$orderCol = $this->state->get('list.ordering', 'a.id');
		$orderDirn = $this->state->get('list.direction', 'desc');

		if ($orderCol == 'a.ordering' || $orderCol == 'category_title')
		{
			$orderCol = 'c.title ' . $orderDirn . ', a.ordering';
		}
                if ($orderCol == 'a.author')
		{
			$orderCol = 'ua.name ' . $orderDirn . ', a.ordering';
		}
		// SQL server change
		if ($orderCol == 'language')
		{
			$orderCol = 'l.title';
		}

		if ($orderCol == 'access_level')
		{
			$orderCol = 'ag.title';
		}

		$query->order($db->escape($orderCol . ' ' . $orderDirn));

		return $query;
	}
                

	/**
	 * Build a list of authors
	 *
	 * @return  JDatabaseQuery
	 *
	 * @since   1.6
	 */
	public function getAuthors()
	{
		// Create a new query object.
		$db = $this->getDbo();
		$query = $db->getQuery(true);

		// Construct the query
		$query->select('u.id AS value, u.name AS text')
			->from('#__users AS u')
			->join('INNER', '#__themensammlung_entry AS c ON c.created_by = u.id')
			->group('u.id, u.name')
			->order('u.name');

		// Setup the query
		$db->setQuery($query);

		// Return the result
		return $db->loadObjectList();
	}
        
        /**
	 * Method to get an array of data items.
	 *
	 * @return	mixed	An array of data items on success, false on failure.
	 *
	 * @since	0.0.1
	 *
	 * @see		JModelList::getItems
	 */
	public function getItems() 
	{
		// Get a storage key.
		$store = $this->getStoreId();

		// Try to load the data from internal storage.
		if (!empty($this->cache[$store])) 
		{
			return $this->cache[$store];
		}

		// Get the items from JModelList
		$items = parent::getItems();                		                
                if ($this->getError()) 
		{
			return false;
		}
		foreach ($items as $item) 
		{
			// Load tags for each entry
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);

			// Select some fields
			$query->select('alternate');
			$query->from('#__themensammlung_entry_tags');
			$query->where('eid=' . $item->id);
			$query->order('ordering');
			$db->setQuery($query);
			$item->tags = $db->loadColumn();
			if ($db->getErrorNum()) 
			{
				$this->setError($db->getErrorMsg());
				return false;
			}
                        
		}
                
                
		// Add the items to the internal cache.
		//$this->cache[$store] = $items;
		//return $this->cache[$store];
                return $items;
	}
}

