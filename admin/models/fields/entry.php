<?php

/**
 * @version		$Id: entry.php 63 2011-04-27 01:35:59Z bfoecke $
 * @package		Themensammlung
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Benjamin F�cke. All rights reserved.
 * @author		Benjamin F�cke
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import formfield and helper from form library
jimport('joomla.form.formfield');
jimport('joomla.form.helper');

// import JFormFieldGroupedList
JFormHelper::loadFieldClass('groupedlist');

/**
 * Entry field of Themensammlung component
 *
 * @since	0.0.1
 */
class JFormFieldEntry extends JFormFieldGroupedList
{
	/**
	 * Method to get the field option groups.
	 *
	 * @return	array	The field option objects as a nested array in groups.
	 *
	 * @since	0.0.1
	 *
	 * @see		JFormFieldGroupedList::getGroups
	 */
	protected function getGroups() 
	{
		// Initialize variables.
		$groups = array();

		// Get the DB object
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Select some fields
		$query->select('e.title as `text`, e.id as value');
		$query->from('#__themensammlung_entry as e');

		// Join over the language
		$query->select('l.title AS language_title');
		$query->join('LEFT', '#__languages AS l ON l.lang_code = e.language');

		// Order by language then by title
		$query->order('language_title ASC');
		$query->order('`text` ASC');

		// Get results
		$db->setQuery($query);
		$results = $db->loadObjectList();
		if ($db->getErrorNum()) 
		{
			// Error in DB. Raise a warning
			JError::raiseWarning($db->getErrorMsg());
			return false;
		}
		foreach ($results as $result) 
		{
			// Add each result to its group
			$groups[$result->language_title][] = $result;
		}
		return $groups;
	}
}
