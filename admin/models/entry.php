<?php

/**
 * @version		$Id: entry.php 63 2011-04-27 01:35:59Z bfoecke $
 * @package		Themensammlung
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Benjamin F�cke. All rights reserved.
 * @author		Benjamin F�cke
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla modeladmin library
jimport('joomla.application.component.modeladmin');

// import Joomla categories library
jimport('joomla.application.categories');

/**
 * Entry Model of Themensammlung component
 *
 * @since	0.0.1
 */
class ThemensammlungModelEntry extends JModelAdmin
{
	/**
	 * @var        string    The prefix to use with controller messages.
	 * @since   1.6
	 */
	protected $text_prefix = 'COM_THEMENSAMMLUNG';

	/**
	 * The type alias for this content type (for example, 'com_content.article').
	 *
	 * @var      string
	 * @since    3.2
	 */
	public $typeAlias = 'com_themensammlung.entry';

	/**
	 * The context used for the associations table
	 *
	 * @var      string
	 * @since    3.4.4
	 */
	protected $associationsContext = 'com_themensammlung.item';
        
        /**
	 * Batch copy items to a new category or current.
	 *
	 * @param   integer  $value     The new category.
	 * @param   array    $pks       An array of row IDs.
	 * @param   array    $contexts  An array of item contexts.
	 *
	 * @return  mixed  An array of new IDs on success, boolean false on failure.
	 *
	 * @since   11.1
	 */
	protected function batchCopy($value, $pks, $contexts)
	{
		$categoryId = (int) $value;

		$newIds = array();

		/*if (!parent::checkCategoryId($categoryId))
		{
			return false;
		}*/

		// Parent exists so we let's proceed
		while (!empty($pks))
		{
			// Pop the first ID off the stack
			$pk = array_shift($pks);

			$this->table->reset();

			// Check that the row actually exists
			if (!$this->table->load($pk))
			{
				if ($error = $this->table->getError())
				{
					// Fatal error
					$this->setError($error);

					return false;
				}
				else
				{
					// Not fatal error
					$this->setError(JText::sprintf('JLIB_APPLICATION_ERROR_BATCH_MOVE_ROW_NOT_FOUND', $pk));
					continue;
				}
			}

			// Alter the title & alias
			$data = $this->generateNewTitle($categoryId, $this->table->alias, $this->table->title);
			$this->table->title = $data['0'];
			$this->table->alias = $data['1'];

			// Reset the ID because we are making a copy
			$this->table->id = 0;

			// Reset hits because we are making a copy
			$this->table->hits = 0;

			// Unpublish because we are making a copy
			$this->table->state = 0;

			// New category ID
			$this->table->catid = $categoryId;

			// TODO: Deal with ordering?
			// $table->ordering = 1;

			// Get the featured state
			//$featured = $this->table->featured;

			// Check the row.
			if (!$this->table->check())
			{
				$this->setError($this->table->getError());
				return false;
			}

			parent::createTagsHelper($this->tagsObserver, $this->type, $pk, $this->typeAlias, $this->table);

			// Store the row.
			if (!$this->table->store())
			{
				$this->setError($this->table->getError());
				return false;
			}

			// Get the new item ID
			$newId = $this->table->get('id');

			// Add the new ID to the array
			$newIds[$pk] = $newId;

			// Check if the article was featured and update the #__content_frontpage table
			/*if ($featured == 1)
			{
				$db = $this->getDbo();
				$query = $db->getQuery(true)
					->insert($db->quoteName('#__content_frontpage'))
					->values($newId . ', 0');
				$db->setQuery($query);
				$db->execute();
			}*/
		}

		// Clean the cache
		$this->cleanCache();

		return $newIds;
	}
        
        /**
	 * Method to test whether a record can be deleted.
	 *
	 * @param	object	$record	A record object.
	 *
	 * @return	boolean	True if allowed to delete the record. Defaults to the permission set in the component.
	 *
	 * @since	0.0.1
	 *
	 * @see		JModelAdmin::canDelete
	 */
	protected function canDelete($record) 
	{
		$user = JFactory::getUser();
		if ($record->catid) 
		{
			return $user->authorise('core.delete', 'com_themensammlung.category.' . (int)$record->catid);
		}
		else
		{
			return parent::canDelete($record);
		}
	}

	/**
	 * Method to test whether a record state can be changed.
	 *
	 * @param	object	$record	A record object.
	 *
	 * @return	boolean	True if allowed to change the state of the record. Defaults to the permission set in the component.
	 *
	 * @since	0.0.1
	 *
	 * @see		JModelAdmin::canEditState
	 */
	protected function canEditState($record) 
	{
		$user = JFactory::getUser();

		// Check for existing article.
		if (!empty($record->id))
		{
			return $user->authorise('core.edit.state', 'com_themensammlung.entry.' . (int) $record->id);
		}
		// New article, so check against the category.
		elseif (!empty($record->catid))
		{
			return $user->authorise('core.edit.state', 'com_themensammlung.category.' . (int) $record->catid);
		}
		// Default to component settings if neither article nor category known.
		else
		{
			return parent::canEditState('com_themensammlung');
		}
	}

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param	string	$type	The table type to instantiate.
	 * @param	string	$prefix	A prefix for the table class name.
	 * @param	array	$config	Configuration array for model. Optional.
	 *
	 * @return	JTable	A database object
	 *
	 * @since	0.0.1
	 *
	 * @see		JModel::getTable
	 */
	public function getTable($type = 'Entry', $prefix = 'ThemensammlungTable', $config = array()) 
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param	array	$data		Data for the form.
	 * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 *
	 * @return	mixed	A JForm object on success, false on failure
	 *
	 * @since	0.0.1
	 *
	 * @see		JModelForm::getForm
	 */
	public function getForm($data = array(), $loadData = true) 
	{
		// Get the form.
		$form = $this->loadForm('com_themensammlung.entry', 'entry', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) 
		{
			return false;
		}

		// Modify the form based on access controls.
		if (!$this->canEditState((object)$data)) 
		{
			// Disable fields for display.
			$form->setFieldAttribute('published', 'disabled', 'true');

			// Disable fields while saving.
			// The controller has already verified this is a record you can edit.

			$form->setFieldAttribute('published', 'filter', 'unset');
		}
                
                // Prevent messing with article language and category when editing existing article with associations
		$app = JFactory::getApplication();
		$assoc = JLanguageAssociations::isEnabled();

		// Check if article is associated
		if ($this->getState('entry.id') && $app->isSite() && $assoc)
		{
			$associations = JLanguageAssociations::getAssociations('com_themensammlung', '#__themensammlung_entry', 'com_themensammlung.entry', $id);

			// Make fields read only
			if ($associations)
			{
				$form->setFieldAttribute('language', 'readonly', 'true');
				$form->setFieldAttribute('catid', 'readonly', 'true');
				$form->setFieldAttribute('language', 'filter', 'unset');
				$form->setFieldAttribute('catid', 'filter', 'unset');
			}
		}
                
		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return	mixed	The data for the form.
	 *
	 * @since	0.0.1
	 *
	 * @see		JModelForm::loadFormData
	 */
	protected function loadFormData() 
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_themensammlung.edit.entry.data', array());
		if (empty($data)) 
		{
			$data = $this->getItem();

			// Set the tags
                        if (!empty($data->tags))
                            $data->tags = implode(', ', $data->tags);

			// Prime some default values.
			if ($data && $this->getState('entry.id') == 0) 
			{
				$app = JFactory::getApplication();
				$data->set('catid', JRequest::getInt('catid', $app->getUserState('com_themensammlung.entries.filter.category_id')));
			}
		}
		return $data;
	}

	/**
	 * Method to get a single record.
	 *
	 * @param	integer	$pk	The id of the primary key.
	 *
	 * @return	mixed	Object on success, false on failure.
	 *
	 * @since	0.0.1
	 *
	 * @see		JModelAdmin::getItem
	 */
	public function getItem($pk = null) 
	{
		if ($item = parent::getItem($pk)) 
		{
			// Convert metadata field to registry.
			$registry = new JRegistry;
			$registry->loadString($item->metadata);
			$item->metadata = $registry->toArray();
                        $user = JFactory::getUser();
			if ($item->id) 
			{
				// Get the database object
				$db = JFactory::getDbo();

				// Get the tags
				$query = $db->getQuery(true);
				$query->select('alternate');
				$query->from('#__themensammlung_entry_tags');
				$query->where('eid=' . (int)$item->id);
				$query->order('ordering');
				$db->setQuery($query);
				$item->tags = $db->loadColumn();
				if ($db->getErrorNum()) 
				{
					$this->setError($db->getErrorMsg());
					return false;
				}
                                
                                // Get the categories
				$query = $db->getQuery(true);
				$query->select('alternativecatid');
				$query->from('#__themensammlung_entry_categories');
				$query->where('eid=' . (int)$item->id);
				$db->setQuery($query);
				$item->alternativecatid = $db->loadColumn();
				if ($db->getErrorNum()) 
				{
					$this->setError($db->getErrorMsg());
					return false;
				}
                                
                                // Get the comments
				$query = $db->getQuery(true);
				$query->select('text2, eid');
				$query->from('#__themensammlung_comments');
				$query->where('eid=' . (int)$item->id);
				$db->setQuery($query);
				$item->text2 = $db->loadColumn();
				if ($db->getErrorNum())
				{
					$this->setError($db->getErrorMsg());
					return false;
				}

                                // Get the Links
				$query = $db->getQuery(true);
				$query->select('id, link, description');
				$query->from('#__themensammlung_entry_links');
				$query->where('eid=' . (int)$item->id);
				$db->setQuery($query);
				$item->links = $db->loadObjectList();
				if ($db->getErrorNum()) 
				{
					$this->setError($db->getErrorMsg());
					return false;
				}
                                
                                // Get the files
				$query = $db->getQuery(true);
				$query->select('id, filename, filesize');
				$query->from('#__themensammlung_entry_pdfs');
				$query->where('eid=' . (int)$item->id);
				$db->setQuery($query);
				$item->pdfs = $db->loadObjectList();
				if ($db->getErrorNum()) 
				{
					$this->setError($db->getErrorMsg());
					return false;
				}
			}
			else
			{
				$item->tags = array();
                                $item->alternativecatid = array();
                                $item->links = array();
                                $item->pdfs = array();
                                $item->comments = array();
			}
                        
		}
                // Load associated content items
		$app = JFactory::getApplication();
		$assoc = JLanguageAssociations::isEnabled();

		if ($assoc)
		{
			$item->associations = array();

			if ($item->id != null)
			{
				$associations = JLanguageAssociations::getAssociations('com_themensammlung', '#__themensammlung_entry', 'com_themensammlung.entry', $item->id);

				foreach ($associations as $tag => $association)
				{
					$item->associations[$tag] = $association->id;
				}
			}
		}
		return $item;
	}

	/**
	 * Method to save the form data.
	 *
	 * @param	array	$data	The form data.
	 *
	 * @return	boolean	True on success.
	 *
	 * @since	0.0.1
	 *
	 * @see		JModelAdmin::save
	 */
	public function save($data) 
	{
            
        	$input = JFactory::getApplication()->input;
		$filter  = JFilterInput::getInstance();

		if (isset($data['metadata']) && isset($data['metadata']['author']))
		{
			$data['metadata']['author'] = $filter->clean($data['metadata']['author'], 'TRIM');
		}

		if (isset($data['created_by_alias']))
		{
			$data['created_by_alias'] = $filter->clean($data['created_by_alias'], 'TRIM');
		}
                
                // Alter the title for save as copy
		if ($input->get('task') == 'save2copy')
		{
			$origTable = clone $this->getTable();
			$origTable->load($input->getInt('id'));

			if ($data['title'] == $origTable->title)
			{
				list($title, $alias) = $this->generateNewTitle($data['catid'], $data['alias'], $data['title']);
				$data['title'] = $title;
				$data['alias'] = $alias;
			}
			else
			{
				if ($data['alias'] == $origTable->alias)
				{
					$data['alias'] = '';
				}
			}

			$data['state'] = 0;
		}
                
                $id = $data['id'];
                $cs_title = $id."-".$data['title'];
                
		// Compte the is_new and the change_language flags
		if ($id) 
		{
			$table = $this->getTable();
			$table->load($id);
			$is_new = false;
			$change_language = $table->language != $data['language'];
		}
		else
		{
			$is_new = true;
			$change_language = false;
		}

		// Automatic handling of alias for empty fields
		if (in_array($input->get('task'), array('apply', 'save', 'save2new')) && (!isset($data['id']) || (int) $data['id'] == 0))
		{
			if ($data['alias'] == null)
			{
				if (JFactory::getConfig()->get('unicodeslugs') == 1)
				{
					$data['alias'] = JFilterOutput::stringURLUnicodeSlug($data['title']);
				}
				else
				{
					$data['alias'] = JFilterOutput::stringURLSafe($data['title']);
				}

				$table = JTable::getInstance('Entry', 'ThemensammlungTable');

				if ($table->load(array('alias' => $data['alias'], 'catid' => $data['catid'])))
				{
					$msg = JText::_('COM_CONTENT_SAVE_WARNING');
				}

				list($title, $alias) = $this->generateNewTitle($data['catid'], $data['alias'], $data['title']);
				$data['alias'] = $alias;

				if (isset($msg))
				{
					JFactory::getApplication()->enqueueMessage($msg, 'warning');
				}
			}
		}

                // If saving is successfull
		if (parent::save($data)) 
		{
			$db = JFactory::getDbo();
			if ($is_new) 
			{
				// Get the new inserted id
				$id = $this->getState('entry.id');
			}
			else
			{
				// Get the current id
				$id = $table->id;

				// Delete the old tags
				$query = $db->getQuery(true);
				$query->delete();
				$query->from('#__themensammlung_entry_tags');
				$query->where('eid=' . $id);
				$db->setQuery($query);
				$db->query();
				if ($db->getErrorNum()) 
				{
					$this->setError($db->getErrorMsg());
					return false;
				}
			}

			// Compute the tags
			$tags = trim($data['tags']);
			if (!empty($tags)) 
			{
				$tags = explode(',', $tags);
			
			// Insert new tags
			$query = $db->getQuery(true);
			$query->insert('#__themensammlung_entry_tags');
			foreach ($tags as $i => $tag) 
			{
				$query->clear('set');
				$query->set('eid=' . (int)$id);
				$query->set('alternate=' . $db->quote(trim(preg_replace('/(\s+)/', ' ', $tag))));
				$query->set('ordering=' . ($i + 1));
				$db->setQuery($query);
				$db->query();
				if ($db->getErrorNum()) 
				{
					$this->setError($db->getErrorMsg());
					return false;
				}
			}
                        }
                        
                        // Compute the categories
                        if (!empty($data['alternativecatid']))
                            $categories = $data['alternativecatid'];
                        
                        // Remove the categories
                        $query = $db->getQuery(true);
                        $query->delete()->from('#__themensammlung_entry_categories')->where('eid = '. $id);
                        $db->setQuery($query);
                        $db->query();
                        
                        if(!empty($categories)){
                            // Insert alternative Categories
                            $query = $db->getQuery(true);
                            $query->insert('#__themensammlung_entry_categories');
                            foreach ($categories as $i => $category) 
                            {
                                    $query->clear('set');
                                    $query->set('eid=' . (int)$id);
                                    $query->set('alternativecatid=' . $db->quote($category));
                                    $db->setQuery($query);
                                    $db->query();
                                    if ($db->getErrorNum()) 
                                    {
                                            $this->setError($db->getErrorMsg());
                                            return false;
                                    }
                            }
                        
                        }

                        // Compute the comments
			$comments = $data['text2'];

                        // Remove the categories
                        $query = $db->getQuery(true);
                        $query->delete()->from('#__themensammlung_comments')->where('eid = '. $id);
                        $db->setQuery($query);
                        $db->query();

                        if(!empty($comments)){
                            
                            // Insert alternative Categories
                            $query = $db->getQuery(true);
                            $query->insert('#__themensammlung_comments');
                            foreach ($comments as $i => $comment)
                            {
                                    $query->clear('set');
                                    $query->set('eid=' . (int)$id);
                                    $query->set('text2=' . $db->quote($comment));
                                    $db->setQuery($query);
                                    $db->query();
                                    if ($db->getErrorNum())
                                    {
                                            $this->setError($db->getErrorMsg());
                                            return false;
                                    }
                            }

                        }
                        
                        // Links speichern
                        $links		= JRequest::getVar('jform', false, false, 'array');
                        if (!empty($links['link'])){
                            $links = $links['link'];                        
                            if(!empty($links) && is_array($links)){
                                // Insert alternative Categories
                                $query = $db->getQuery(true);
                                $query->insert('#__themensammlung_entry_links');
                                foreach ($links as $link) 
                                {
                                        $query->clear('set');
                                        $query->set('eid=' . (int)$id);
                                        $query->set('link=' . $db->quote($link['link']));
                                        $query->set('description=' . $db->quote($link['description']));
                                        $db->setQuery($query);
                                        $db->query();
                                        if ($db->getErrorNum()) 
                                        {
                                                $this->setError($db->getErrorMsg());
                                                return false;
                                        }
                                }
                            }
                        }
                        
                        // Dateien speichern
                        $files		= JRequest::getVar('jform', '', 'files', 'array');

                        //$this->setError('Leer');
                        
                        for($i=0; $i<count($files['tmp_name']['pdfupload']); $i++){
                            
                            $file_name = $files['name']['pdfupload'][$i];
                            $file_type = $files['type']['pdfupload'][$i];
                            $file_tmpname = $files['tmp_name']['pdfupload'][$i];
                            $file_error = $files['error']['pdfupload'][$i];
                            $file_size = $files['size']['pdfupload'][$i];
                            if ($file_tmpname!=""){

                            if($file_type == 'application/pdf' || substr($file_name,-3) == 'pdf' || substr($file_name,-3) == '.PDF' || $file_type == 'application/word' || substr($file_name,-3) == 'doc' || substr($file_name,-3) == '.DOC' || substr($file_name,-3) == 'docx' || substr($file_name,-3) == '.DOCX' || substr($file_name,-3) == 'ppt' || substr($file_name,-3) == '.PPT'){
                                $file_type_error = false;
                            }else{
                                $file_type_error = true;
                            }
                            
                            if($file_type_error){
                                JError::raiseNotice(100, JText::_('Die hochzuladende Datei ist weder keine PDF-, Word- oder PowerPoint-Datei!').': '.$file_name.' ('.$file_type.')');
                                continue;
                            }
                            
                            if($file_error != 0 || !file_exists($file_tmpname)){
                                JError::raiseNotice(100, JText::_('COM_THEMENSAMMLUNG_WARNING_UPLOAD_FILE_NOT_EXIST').': '.$file_name);
                                continue;                                
                            }
                            $time = mktime(); 

                            /**
                             * oeffnen und auslesen der PDF-Datei
                             */
                            
                            $handle = fopen($file_tmpname, 'r');                            // Pdf-Datei �ffnen
                            $raw_data = addslashes(fread($handle, filesize($file_tmpname)));            // Inhalt auslesen
                            fclose($handle);  

                            /*
                             * Pdf in Text umwandeln (nur mit schreiben in eine Datei m�glich
                             * und auslesen deiser Datei
                             */
                            $params =& JComponentHelper::getParams('com_themensammlung');
                            $pdftotext_installed = $params->get('data.pdftotext_installed');
                            if ($pdftotext_installed=='1'){
                                system("pdftotext -layout -enc 'UTF-8' -nopgbrk {$file_tmpname} {$time}.txt");       // Umwandeln der PDF in Text
                                sleep(2);
                                if(file_exists($time.".txt")){
                                    $handle = fopen($time.".txt", 'r');                         // Neu geschriebene Datei �ffnen
                                    $parsed_data = fread($handle, filesize($time.'.txt'));      // Inhalt auslesen
                                    fclose($handle);                                            // Datei-Handel schlie�en
                                    unlink($time.'.txt');
                                }else{
                                    JError::raiseNotice(100, JText::_('COM_THEMENSAMMLUNG_WARNING_PDFTOTEXT_IS_NOT_WORKING_PROPERLY'));
                                    $parsed_data = "";
                                }
                            }
                            else {
                                $parsed_data = "";
                            }
                         
                            
                            /**
                             * Daten in die Datenbank schreiben
                             */
                            $query = $db->getQuery(true);
                            $query->insert('#__themensammlung_entry_pdfs');
                            $query->clear('set');
                            $query->set("eid=" . (int)$id);
                            $query->set("filename='" . $db->escape($file_name) . "'");
                            $query->set("raw_data='" . $raw_data . "'");
                            $query->set("parsed_data='" . $db->escape($parsed_data) . "'");
                            $query->set("filesize=" . $file_size );
                            $db->setQuery($query);
                            $db->query();
                            if ($db->getErrorNum()) 
                            {
                                    $this->setError($db->getErrorMsg());
                                    return false;
                            }


                        }}
                                                
                        
			return true;
		}
		return false;
	}

	/**
	 * A protected method to get a set of ordering conditions.
	 *
	 * @param	object	$table	A JTable object.
	 *
	 * @return	array	An array of conditions to add to ordering queries.
	 *
	 * @since	0.0.1
	 *
	 * @see		JModelAdmin::getReorderConditions
	 */
	protected function getReorderConditions($table) 
	{
		$condition = array();
		$condition[] = 'catid = ' . (int)$table->catid;
		return $condition;
	}
}
