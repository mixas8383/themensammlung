-- @version		$Id: script.php 63 2011-04-27 01:35:59Z bfoecke $
-- @package		Themensammlung
-- @subpackage	Component
-- @copyright	Copyright (C) 2010-2011 Benjamin F�cke. All rights reserved.
-- @author		Benjamin F�cke
-- @link		http://www.veasy.de
-- @license		http://www.gnu.org/licenses/gpl-2.0.html

--
-- Tabellenstruktur fuer Tabelle `jos_themensammlung_entry`
--

CREATE TABLE IF NOT EXISTS `#__themensammlung_entry` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `title` varchar(255) NOT NULL DEFAULT '',
  `subtitle` varchar(255) DEFAULT NULL,
  `alias` varchar(255) NOT NULL DEFAULT '',
  `text` text NOT NULL,
  `literature` varchar(255) DEFAULT NULL,
  `continuative` varchar(255) DEFAULT NULL,
  `published` tinyint(3) NOT NULL DEFAULT '0',
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `author` int(10) NOT NULL,
  `author_alias` varchar(255) NOT NULL,
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` mediumtext NOT NULL,
  `metadata` mediumtext NOT NULL,
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL COMMENT 'The language code for the definition.',
  `introduction` int(11) NOT NULL,
  `text2` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_published` (`published`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`),
  KEY `idx_ordering` (`ordering`),
  KEY `idx_language` (`language`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur f r Tabelle `jos_themensammlung_entry_categories`
--

CREATE TABLE IF NOT EXISTS `#__themensammlung_entry_categories` (
  `eid` int(10) NOT NULL,
  `alternativecatid` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur f r Tabelle `jos_themensammlung_comments`
--

CREATE TABLE IF NOT EXISTS `#__themensammlung_comments` (
  `eid` int(10) NOT NULL,
  `text2` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------
--
-- Tabellenstruktur f r Tabelle `jos_themensammlung_entry_links`
--

CREATE TABLE IF NOT EXISTS `#__themensammlung_entry_links` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `eid` int(10) NOT NULL,
  `link` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur f r Tabelle `jos_themensammlung_entry_pdfs`
--

CREATE TABLE IF NOT EXISTS `#__themensammlung_entry_pdfs` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `eid` int(10) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `filesize` mediumint(9) NOT NULL,
  `raw_data` longblob NOT NULL,
  `parsed_data` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur f r Tabelle `jos_themensammlung_entry_tags`
--

CREATE TABLE IF NOT EXISTS `#__themensammlung_entry_tags` (
  `eid` int(10) unsigned NOT NULL COMMENT 'FK to the #__themensammlung_entry table',
  `alternate` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  KEY `idx_eid` (`eid`),
  KEY `idx_alternate` (`alternate`),
  KEY `idx_ordering` (`ordering`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;