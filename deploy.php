<?php
require_once 'vendor/autoload.php';

use Comodojo\Zip\Zip;
use Spatie\ArrayToXml\ArrayToXml;

// Script example.php
$shortopts = "";
$shortopts .= "v:";  // Required value



$longopts = array(
    "version:", // Required value
);




$options = getopt($shortopts, $longopts);


$verS = file_get_contents('version.json');
if (!empty($verS))
{
    $versionObject = json_decode($verS);
} else
{
    die('Exit : version file empty');
}
$version = $versionObject->next_version;

if (!is_dir('./release'))
{
    mkdir('./release');
}


$fileName = 'release/' . $versionObject->component_name . '_' . $versionObject->next_version . '.zip';
$downloadUrl = 'https://api.bitbucket.org/2.0/repositories/' . $versionObject->repository_owner . '/' . $versionObject->repository . '/downloads/' . $versionObject->component_name . '_' . $versionObject->next_version . '.zip';
if (file_exists($fileName))
{
    unlink($fileName);
}

// make and save update manifest xml
$updateScrip = [
    "update" => [
        "name" => $versionObject->component_name,
        "description" => $versionObject->component_name,
        "element" => $versionObject->component_name,
        "type" => "component",
        "version" => $versionObject->next_version,
        "infourl" => ["_attributes" => ["title" => "themensammlung", "_value" => "https://www.big5.de"]],
        "downloads" => [
            "downloadurl" => [
                "_attributes" => ["type" => "full", "format" => "zip"]
                , "_value" => $downloadUrl
            ]
        ],
        "maintainer" => "BIG 5 Concepts GmbH",
        "maintainerurl" => "https://www.big5.de",
        "tags" => ["tag" => 'stable'],
        "targetplatform" => ["_attributes" => ["name" => "joomla", "version" => "3.[456789]"]]
    ]
];


//
$t = ArrayToXml::convert($updateScrip, 'updates');
$simpleXml = new SimpleXMLElement($t);
$dom = dom_import_simplexml($simpleXml)->ownerDocument;
$dom->formatOutput = true;
$dom->saveXML();
$dom->save('release/update-manifest.xml');


//load, change version and save component instalation manifest
$dom = new DOMDocument();
$dom->load($versionObject->component_manifest);
$root = $dom->documentElement;
$nodesToDelete = array();
$componentVersion = $root->getElementsByTagName('version');
$componentVersion->item(0)->textContent = $versionObject->next_version;

$componentVersion = $root->getElementsByTagName('server');
$componentVersion->item(0)->textContent = 'https://api.bitbucket.org/2.0/repositories/'.$versionObject->repository_owner.'/'.$versionObject->repository.'/downloads/update-manifest.xml';

$dom->saveXML();
$dom->save($versionObject->component_manifest);



//creat zip archive of component
$zip = Zip::create($fileName);
$files = $versionObject->files;
foreach ($files as $one)
{
    $zip->add($one);
}

$zip->close();

//update version in version.json file and put version in temporary version.txt file to get in pipeline
$versionObject->current_version = $versionObject->next_version;
$versionTmp = explode('.', $versionObject->next_version);
$versionTmp[count($versionTmp) - 1] = $versionTmp[count($versionTmp) - 1] + 1;
$versionObject->next_version = implode('.', $versionTmp);

file_put_contents('version.json', json_encode($versionObject));
file_put_contents('version.txt', $versionObject->current_version);



