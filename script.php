<?php
/**
 * @version		$Id: script.php 63 2011-04-27 01:35:59Z bfoecke $
 * @package		Themensammlung
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Benjamin F�cke. All rights reserved.
 * @author		Benjamin F�cke
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Script file of Themensammlung component
 */
class com_themensammlungInstallerScript
{

    protected $old_version;

    /**
     * method to install the component
     *
     * @param	JInstallerComponent	$parent	The class calling this method
     *
     * @return void
     */
    function install($parent)
    {
        $buffer = '';
        $manifest = $parent->get("manifest");
        $parent = $parent->getParent();
        $source = $parent->getPath("source");

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        if (count($manifest->plugins->plugin) > 0)
        {
            // Opening HTML
            ob_start();
            ?>
            <tr class="workinstall-subheading">
                <th>
                    <?php echo JText::_('COM_WORK_PLUGIN_HEADER'); ?>
                </th>
                <th>
                    <?php echo JText::_('COM_WORK_PLUGIN_GROUP_HEADER'); ?>
                </th>				
                <th width="50%">
                    <?php echo JText::_('COM_WORK_STATUS_HEADER'); ?>
                </th>					
            </tr>

            <?php
            $buffer .= ob_get_clean();

            foreach ($manifest->plugins->plugin as $plugin)
            {
                $attributes = $plugin->attributes();
                $plg = $source . '/' . $attributes['folder'] . '/' . $attributes['plugin'];

                $installer = new JInstaller();

                if (!$installer->install($plg))
                {
                    $error_msg = '';
                    while ($error = JError::getError(true))
                    {
                        $error_msg .= $error;
                        $install_error = true;
                    }
                    $buffer .= $this->printError($attributes['plugin'], $attributes['group'], 'install', $error_msg);
                    //$this->abort();
                    break;
                } else
                {
                    $buffer .= $this->printSuccess($attributes['plugin'], $attributes['group'], 'install');
                }

                $query->clear();
                $query->update($db->quoteName('#__extensions'));
                //Set any other field values as required
                $query->set($db->quoteName('enabled') . ' = 1');
                $query->where($db->quoteName('name') . ' = ' . $db->quote($attributes['plugin']));
                $query->where($db->quoteName('type') . ' = ' . $db->quote('plugin'));
                $db->setQuery($query->__toString());
                try
                {
                    $db->execute();
                    $buffer .= $this->printSuccess($attributes['plugin'], $attributes['group'], 'publish');
                } catch (RuntimeException $e)
                {
                    $install_error = true;
                    $buffer .= $this->printError($attributes['plugin'], $attributes['group'], 'publish', $e->getMessage());
                }
            }
        }
        // Install modules
        if (count($manifest->modules->module) > 0)
        {
            // Opening HTML
            ob_start();
            ?>
            <tr class="workinstall-subheading">
                <th>
                    <?php echo JText::_('COM_WORK_MODULE_HEADER'); ?>
                </th>
                <th>
                    <?php echo JText::_('COM_WORK_MODULE_GROUP_HEADER'); ?>
                </th>				
                <th width="50%">
                    <?php echo JText::_('COM_WORK_STATUS_HEADER'); ?>
                </th>					
            </tr>

            <?php
            $buffer .= ob_get_clean();

            foreach ($manifest->modules->module as $module)
            {
                $error_msg = '';
                $attributes = $module->attributes();
                $mod = $source . '/' . $attributes['folder'] . '/' . $attributes['module'];

                $installer = new JInstaller();

                if (!$installer->install($mod))
                {
                    while ($error = JError::getError(true))
                    {
                        $error_msg .= $error;
                        $install_error = true;
                    }
                    //  $buffer .= $this->printError($attributes['module'], 'site', 'install', $error_msg);
                    //$this->abort();
                    break;
                } else
                {
                    //  $buffer .= $this->printSuccess($attributes['module'], 'site', 'install');
                }
            }
        }
    }

    /**
     * method to uninstall the component
     *
     * @param	JInstallerComponent	$parent	The class calling this method
     *
     * @return void
     */
    function uninstall($parent)
    {
        // Get the DB object
        $db = JFactory::getDbo();

        // Delete all categories
        $query = $db->getQuery(true);
        $query->delete();
        $query->from('#__categories');
        $query->where('extension=' . $db->quote('com_themensammlung'));
        $db->setQuery($query);
        $db->query();
        if ($db->getErrorNum())
        {
            echo JText::sprintf('JLIB_DATABASE_ERROR_FUNCTION_FAILED', $db->getErrorNum(), $db->getErrorMsg());
            return;
        }
        $table = JTable::getInstance('Category');
        $table->rebuild();
    }

    /**
     * method to update the component
     *
     * @param	JInstallerComponent	$parent	The class calling this method
     *
     * @return void
     */
    function update($parent)
    {
        $buffer = '';

        $manifest = $parent->get("manifest");
        $parent = $parent->getParent();
        $source = $parent->getPath("source");

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);


        // Get the new version
        $new_version = $parent->getManifest()->version;
        if ($this->old_version && $this->old_version == '0.0.1' && version_compare($new_version, '0.0.1') > 0)
        {
            // Get the DB object
            $db = JFactory::getDbo();

            // Get all entry tables
            $query = 'SHOW TABLES LIKE ' . $db->quote($db->getPrefix() . 'themensammlung_entry_%');
            $db->setQuery($query);
            $tables = $db->loadColumn();

            // Check for a database error.
            if ($db->getErrorNum())
            {
                echo JText::sprintf('JLIB_DATABASE_ERROR_FUNCTION_FAILED', $db->getErrorNum(), $db->getErrorMsg());
                return;
            }

            // Update all entry tables
            if (!empty($tables))
            {
                foreach ($tables as $table)
                {
                    $query = 'ALTER TABLE `' . $table . '` DROP INDEX `idx_alternate`, ADD INDEX `idx_alternate` (`alternate`)';
                    $db->setQuery($query);
                    $db->query();

                    // Check for a database error.
                    if ($db->getErrorNum())
                    {
                        echo JText::sprintf('JLIB_DATABASE_ERROR_FUNCTION_FAILED', $db->getErrorNum(), $db->getErrorMsg());
                        return;
                    }
                }
            }
        }
        if (count($manifest->plugins->plugin) > 0)
        {
            // Opening HTML
            ob_start();

            $buffer .= ob_get_clean();

            foreach ($manifest->plugins->plugin as $plugin)
            {
                $attributes = $plugin->attributes();
                $plg = $source . '/' . $attributes['folder'] . '/' . $attributes['plugin'];

                // check if the plugin is a new version for this externsion or a new plugin and either update or install
                $query->clear();
                $query->select($db->quoteName('extension_id'));
                $query->from($db->quoteName('#__extensions'));
                $query->where($db->quoteName('name') . ' = ' . $db->quote($attributes['plugin']));
                $query->where($db->quoteName('type') . ' = ' . $db->quote('plugin'));
                $db->setQuery($query->__toString());

                $plg_id = $db->loadResult();
                if ($plg_id)
                {
                    $plg_type = 'update';
                } else
                {
                    $plg_type = 'install';
                }

                $installer = new JInstaller();

                if (!$installer->$plg_type($plg))
                {
                    $error_msg = '';
                    while ($error = JError::getError(true))
                    {
                        $error_msg .= $error;
                        $install_error = true;
                    }
                    // $buffer .= $this->printError($attributes['plugin'], $attributes['group'], $plg_type, $error_msg);
                    //$this->abort();
                    break;
                } else
                {
                    // $buffer .= $this->printSuccess($attributes['plugin'], $attributes['group'], $plg_type);
                }
                if ($plg_type == 'install')
                {

                    $query->clear();
                    $query->update($db->quoteName('#__extensions'));
                    //Set any other field values as required
                    $query->set($db->quoteName('enabled') . ' = 1');
                    $query->where($db->quoteName('name') . ' = ' . $db->quote($attributes['plugin']));
                    $query->where($db->quoteName('type') . ' = ' . $db->quote('plugin'));
                    $db->setQuery($query->__toString());

                    try
                    {
                        $db->execute();
                        // $buffer .= $this->printSuccess($attributes['plugin'], $attributes['group'], 'publish');
                    } catch (RuntimeException $e)
                    {
                        $install_error = true;
                        //$buffer .= $this->printError($attributes['plugin'], $attributes['group'], 'publish', $e->getMessage());
                    }
                }
            }
        }

        // Install modules
        if (count($manifest->modules->module) > 0)
        {
            // Opening HTML
            ob_start();

            $buffer .= ob_get_clean();

            foreach ($manifest->modules->module as $module)
            {
                $error_msg = '';
                $attributes = $module->attributes();
                $mod = $source . '/' . $attributes['folder'] . '/' . $attributes['module'];

                // check if the module is a new version for this externsion or a new plugin and either update or install
                $query->clear();
                $query->select($db->quoteName('extension_id'));
                $query->from($db->quoteName('#__extensions'));
                $query->where($db->quoteName('name') . ' = ' . $db->quote($attributes['module']));
                $query->where($db->quoteName('type') . ' = ' . $db->quote('module'));
                $db->setQuery($query->__toString());

                $mod_id = $db->loadResult();
                if ($mod_id)
                {
                    $mod_type = 'update';
                } else
                {
                    $mod_type = 'install';
                }

                $installer = new JInstaller();

                if (!$installer->$mod_type($mod))
                {
                    while ($error = JError::getError(true))
                    {
                        $error_msg .= $error;
                        $install_error = true;
                    }
                    //  $buffer .= $this->printError($attributes['module'], 'site', $mod_type, $error_msg);
                    //$this->abort();
                    break;
                } else
                {
                    // $buffer .= $this->printSuccess($attributes['module'], 'site', $mod_type);
                }
            }
        }
    }

    /**
     * method to run before an install/update/discover_install method
     *
     * @param	JInstallerComponent	$parent	The class calling this method
     * @param 	string				$type  	The type of change (install, update or discover_install)
     *
     * @return void
     */
    function preflight($type, $parent)
    {
        if ($type == 'update')
        {
            // Get the DB object
            $db = JFactory::getDbo();

            // Get a new query
            $query = $db->getQuery(true);
            $query->select('s.version_id');
            $query->from('#__extensions AS e');
            $query->leftJoin('#__schemas AS s ON e.extension_id=s.extension_id');
            $query->where('e.element=' . $db->quote('com_themensammlung'));
            $db->setQuery($query);
            $this->old_version = $db->loadResult();
        }
    }

    /**
     * method to run after an install/update/discover_install method
     *
     * @param	JInstallerComponent	$parent	The class calling this method
     * @param 	string				$type  	The type of change (install, update or discover_install)
     *
     * @return void
     */
    function postflight($type, $parent)
    {
        // Redirect to the component main view after installing
        // Redirect to the component main view after installing


        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);


        // $installer->install($parent->getParent()->getPath('source').'/'.'plugins'.'/'.'categoryreplacethemen');
        //$installer->install($parent->getParent()->getPath('source').'/'.'modules'.'/'.'mod_joeswordcloud_v3_2_3');
        //$parent->getParent()->setRedirectURL('index.php?option=com_coursedatabase');
        // Enable plugins
        $db = JFactory::getDBO();
        $db->setQuery("UPDATE #__extensions SET enabled = 1 WHERE " .
                "element = 'themensammlungentry' ");
        $db->query();
        $db->setQuery("UPDATE #__extensions SET enabled = 1 WHERE " .
                "element = 'themencontact' ");
        $db->query();
        $db->setQuery("UPDATE #__extensions SET enabled = 1 WHERE " .
                "element = 'themensammlung' ");
        $db->query();
        $db->setQuery("UPDATE #__extensions SET enabled = 1 WHERE " .
                "element = 'categoryreplacethemen' ");
        $db->query();
    }

    private function getManifest()
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        // Construct the query
        $query->select($db->quoteName('manifest_cache'));
        $query->from($db->quoteName('#__extensions'));
        $query->where($db->quoteName('name') . ' = ' . $db->quote('com_themensammlung'));
        $query->where($db->quoteName('type') . ' = ' . $db->quote('component'));

        $db->setQuery($query->__toString());

        $manifest = json_decode($db->loadResult(), true);
        return $manifest;
    }

    private function getParams()
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        // Construct the query
        $query->select($db->quoteName('params'));
        $query->from($db->quoteName('#__extensions'));
        $query->where($db->quoteName('name') . ' = ' . $db->quote('com_themensammlung'));
        $query->where($db->quoteName('type') . ' = ' . $db->quote('component'));

        $db->setQuery($query->__toString());

        $params = json_decode($db->loadResult(), true);
        return $params;
    }

    /**
     * sets parameter values in the component's row of the extension table
     * 
     * @param	array	param array for the extension
     * 
     */
    private function setParams($param_array)
    {
        if (count($param_array) > 0)
        {
            // read the existing component value(s)
            $db = JFactory::getDbo();
            $params = $this->getParams();
            // add the new variable(s) to the existing one(s)
            foreach ($param_array as $name => $value)
            {
                $params[(string) $name] = (string) $value;
            }
            // store the combined new and existing values back as a JSON string
            $params_string = json_encode($params);

            $query = $db->getQuery(true);
            $query->update($db->quoteName('#__extensions'));
            $query->set($db->quoteName('params') . ' = ' . $db->quote($params_string));
            $query->where($db->quoteName('name') . ' = ' . $db->quote('com_themensammlung'));
            $query->where($db->quoteName('type') . ' = ' . $db->quote('component'));

            $db->setQuery($query->__toString());

            $db->execute();
        }
    }

}
