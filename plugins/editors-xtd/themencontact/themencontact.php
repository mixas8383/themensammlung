<?php
/**
 * @version		$Id: entry.php 21097 2011-04-07 15:38:03Z dextercowley $
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.plugin.plugin');

/**
 * Editor Entry buton
 *
 * @package		Joomla.Plugin
 * @subpackage	Editors-xtd.entry
 * @since 1.5
 */
class plgButtonThemencontact extends JPlugin
{
	/**
	 * Constructor
	 *
	 * @access      protected
	 * @param       object  $subject The object to observe
	 * @param       array   $config  An array that holds the plugin configuration
	 * @since       1.5
	 */
	public function __construct(& $subject, $config)
	{
		parent::__construct($subject, $config);
		$this->loadLanguage();
	}


	/**
	 * Display the button
	 *
	 * @return array A four element array of (entry_id, entry_title, category_id, object)
	 */
	function onDisplay($name)
	{
		/*
		 * Javascript to insert the link
		 * View element calls jSelectEntry when an entry is clicked
		 * jSelectEntry creates the link tag, sends it to the editor,
		 * and closes the select frame.
		 */
		$js = "
		function jSelectThemencontact(id, title) {
			var tag = '<a href=\"index.php?option=com_contact&view=contact&id='+id+'\">'+title+'</a>';
			jInsertEditorText(tag, '".$name."');
			SqueezeBox.close();
		}";

		$doc = JFactory::getDocument();
		$doc->addScriptDeclaration($js);

		JHtml::_('behavior.modal');

		/*
		 * Use the built-in element view to select the entry.
		 * Currently uses blank class.
		 */
		$link = 'index.php?option=com_contact&amp;view=contacts&amp;layout=modal2&amp;tmpl=component&amp;function=jSelectThemencontact';

		$button = new JObject();
		$button->set('modal', true);
		$button->set('link', $link);
		$button->set('text', JText::_('PLG_CONTACT_BUTTON_CONTACT'));
		$button->set('name', 'article');
		$button->set('options', "{handler: 'iframe', size: {x: 770, y: 400}}");

		return $button;
	}
}
