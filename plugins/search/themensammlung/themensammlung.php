<?php
/**
 * @version		$Id: content.php 20196 2011-01-09 02:40:25Z ian $
 * @package		Joomla
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

//jimport('joomla.plugin.plugin');

//require_once JPATH_SITE.'/components/com_content/router.php';
require_once JPATH_SITE.'/components/com_themensammlung/helpers/route.php';

/**
 * Content Search plugin
 *
 * @package		Joomla
 * @subpackage	Search
 * @since		1.6
 */
class plgSearchThemensammlung extends JPlugin
{
	
        public function __construct(& $subject, $config)
	{
		parent::__construct($subject, $config);
		$this->loadLanguage();
	}
        
        /**
	 * @return array An array of search areas
	 */
	function onContentSearchAreas()
	{
		static $areas = array(
			'themensammlung' => 'Fachbeitr&auml;ge'
			);
			return $areas;
	}

	/**
	 * Content Search method
	 * The sql must return the following fields that are used in a common display
	 * routine: href, title, section, created, text, browsernav
	 * @param string Target search string
	 * @param string mathcing option, exact|any|all
	 * @param string ordering option, newest|oldest|popular|alpha|category
	 * @param mixed An array if the search it to be restricted to areas, null if search all
	 */
	function onContentSearch($text, $phrase='', $ordering='', $areas=null)
	{
		$db = JFactory::getDbo();
		$app = JFactory::getApplication();
		$user	= JFactory::getUser();
		$groups	= implode(',', $user->getAuthorisedViewLevels());
                $searchText = $text;

		
		//require_once JPATH_SITE.'/administrator/components/com_search/helpers/search.php';

		// If the array is not correct, return it:
		if (is_array( $areas )) {
			if (!array_intersect( $areas, array_keys( $this->onContentSearchAreas() ) )) {
				return array();
			}
		}

		$sContent		= $this->params->get('search_content',		1);
		$sArchived		= $this->params->get('search_archived',		1);
		$limit			= $this->params->def('search_limit',		50);
		
		// Use the PHP function trim to delete spaces in front of or at the back of the searching terms
		$text = trim( $text );
 
		// Return Array when nothing was filled in.
		if ($text == '') {
			return array();
		}

		$wheres = array();
                
		switch ($phrase) {
                        // Search exact
			case 'exact':
				$text = $db->quote('%' . $db->escape($text, true) . '%', false);
				$wheres2	= array();
				$wheres2[]	= 'a.title LIKE '.$text;
				$wheres2[]	= 'a.subtitle LIKE '.$text;
				$wheres2[]	= 'a.text LIKE '.$text;                                
				$wheres2[]	= 'a.metakey LIKE '.$text;
				$wheres2[]	= 'a.metadesc LIKE '.$text;
                                $wheres2[]	= 'd.alternate LIKE '.$text;
                                $wheres2[]	= 'e.name LIKE '.$text;                                
				$where		= '(' . implode(') OR (', $wheres2) . ')';
				break;
                            
                        // Search all or any
			case 'all':
			case 'any':
                            
                        // Set default
			default:
				$words = explode(' ', $text);
				$wheres = array();
				foreach ($words as $word) {
					$word		= $db->quote('%' . $db->escape($word, true) . '%', false);
					$wheres2	= array();
					$wheres2[]	= 'a.title LIKE '.$word;
					$wheres2[]	= 'a.subtitle LIKE '.$word;
					$wheres2[]	= 'a.text LIKE '.$word;                                        
					$wheres2[]	= 'a.metakey LIKE '.$word;
					$wheres2[]	= 'a.metadesc LIKE '.$word;
                                        $wheres2[]	= 'd.alternate LIKE '.$word;
                                        $wheres2[]	= 'e.name LIKE '.$word;
					$wheres[]	= implode(' OR ', $wheres2);
				}
				$where = '(' . implode(($phrase == 'all' ? ') AND (' : ') OR ('), $wheres) . ')';
				break;
		}
                
		$morder = '';
		switch ($ordering) {
			case 'oldest':
				$order = 'a.created ASC';
				break;

			case 'popular':
				$order = 'a.hits DESC';
				break;

			case 'alpha':
				$order = 'a.title ASC';
				break;

			case 'category':
				$order = 'c.title ASC, a.title ASC';
				$morder = 'a.title ASC';
				break;

			case 'newest':
			default:
				$order = 'a.created DESC';
				break;
		}

		$rows = array();
		$query	= $db->getQuery(true);

		// search articles
		if ($sContent && $limit > 0)
		{
			$query->clear();
			$query->select('a.title AS title, a.subtitle, a.metadesc, a.metakey, a.created AS created, e.name AS name2, d.alternate as alternate, '
						.'a.text AS text, a.subtitle AS subtitle, c.title AS section, '
						.'CASE WHEN CHAR_LENGTH(a.alias) THEN CONCAT_WS(":", a.id, a.alias) ELSE a.id END as slug, '
						.'CASE WHEN CHAR_LENGTH(c.alias) THEN CONCAT_WS(":", c.id, c.alias) ELSE c.id END as catslug, '
						.'"2" AS browsernav,a.publish_down');
			$query->from('#__themensammlung_entry a');
            $current_date = date( "Y-m-j H:i:00", strtotime( 'now' ) );
            $query->where( '(' . $db->quoteName('a.publish_down') . ' >= ' . $db->quote( $current_date ) . ')'
                . ' OR (' .  $db->quoteName('a.publish_down') . ' = ' . $db->quote( '0000-00-00 00:00:00' ) . ')' );
                        $query->leftJoin('#__themensammlung_entry_tags d ON a.id=d.eid');
			$query->innerJoin('#__categories c ON c.id=a.catid');
                        
                        $query->leftJoin('#__contact_details e ON e.id = a.author');
                        $query->where('('. $where .')'. 'AND a.published = 1 AND a.access IN ('.$groups.') '
						.'AND c.access IN ('.$groups.') ' );
			$query->group('a.id');
			$query->order($order);

			// Filter by language
			if ($app->isSite() && $app->getLanguageFilter()) {
				$query->where('a.language in (' . $db->Quote($tag) . ',' . $db->Quote('*') . ')');
				$query->where('c.language in (' . $db->Quote($tag) . ',' . $db->Quote('*') . ')');
			}

			$db->setQuery($query, 0, $limit);
			$list = $db->loadObjectList();
                                            
			$limit -= count($list);

			if (isset($list))
			{
				foreach($list as $key => $item)
				{
					$list[$key]->href = ThemensammlungHelperRoute::getEntryRoute($item->slug, $item->catslug);
				}
			}
			$rows[] = $list;
		}
                
		$results = array();
		if (count($rows))
		{
			foreach($rows as $row)
			{
				$new_row = array();
				foreach($row AS $key => $article) {
					if (searchHelper::checkNoHTML($article, $searchText, array('text', 'title', 'subtitle', 'metadesc', 'metakey'))) {
						$new_row[] = $article;
					}
				}
				$results = array_merge($results, (array) $new_row);
			}
		}

                $wheres = array();
		switch ($phrase) {
			case 'exact':
				$text		= $db->quote('%' . $db->escape($text, true) . '%', false);
				$wheres2	= array();
                                $wheres2[]	= 'a.filename LIKE '.$text;
                                $wheres2[]	= 'a.parsed_data LIKE '.$text;
				$where		= '(' . implode(') OR (', $wheres2) . ')';
				break;

			case 'all':
			case 'any':
			default:
				$words = explode(' ', $text);
				$wheres = array();
				foreach ($words as $word) {
					$word		= $db->quote('%' . $db->escape($word, true) . '%', false);
					$wheres2	= array();
                                        $wheres2[]	= 'a.filename LIKE '.$word;
                                        $wheres2[]	= 'a.parsed_data LIKE '.$word;
					$wheres[]	= implode(' OR ', $wheres2);
				}
				$where = '(' . implode(($phrase == 'all' ? ') AND (' : ') OR ('), $wheres) . ')';
				break;
		}

		$morder = '';
		switch ($ordering) {
			case 'oldest':
			case 'popular':
			case 'alpha':
                            default:
				$order = 'a.filename ASC';
				break;
		}

		$rows = array();
		$query	= $db->getQuery(true);

		// search articles
		if ($sContent && $limit > 0)
		{
			$query->clear();
			$query->select('a.filename AS title, a.id AS id, c.title AS title2, '
						.'CONCAT(a.parsed_data) AS text, '
						.'CASE WHEN CHAR_LENGTH(c.alias) THEN CONCAT_WS(":", c.id, c.alias) ELSE c.id END as slug, '
						.'CASE WHEN CHAR_LENGTH(d.alias) THEN CONCAT_WS(":", d.id, d.alias) ELSE d.id END as catslug, '
						.'"2" AS browsernav');
			$query->from('#__themensammlung_entry_pdfs a');
                        $query->leftJoin('#__themensammlung_entry c ON c.id=a.eid');
                        $query->innerJoin('#__categories d ON d.id=c.catid');
                        $query->where('('. $where .')'. 'AND c.published = 1 AND c.access IN ('.$groups.') ');
                        
			$db->setQuery($query, 0, $limit);
			$list = $db->loadObjectList();

			$limit -= count($list);

			if (isset($list))
			{
				foreach($list as $key => $item)
				{

                                    //Create the link
                                    $link = 'pdf_show.php?id=' . $item->id;
                                    $list[$key]->href2 = ThemensammlungHelperRoute::getEntryRoute($item->slug, $item->catslug);
                                    /*if ($item = ThemensammlungHelperRoute::_findItem($needles))
                                    {
                                        $link.= '&Itemid=' . $item;
                                    }
                                    elseif ($item = ThemensammlungHelperRoute::_findItem())
                                    {
                                        $link.= '&Itemid=' . $item;
                                    }*/

					$list[$key]->href = $link;
				}
			}
			$rows[] = $list;
		}

		$results2 = array();
                if (count($rows))
		{
			foreach($rows as $row)
			{
				$new_row = array();
				foreach($row AS $key => $article) {
					if (searchHelper::checkNoHTML($article, $searchText, array('text', 'title', 'title2'))) {
						$new_row[] = $article;
					}
				}
				$results2 = array_merge($results2, (array) $new_row);
			}
		}
                
		$results3 = array_merge($results, $results2);

		return $results3;
	}
}
