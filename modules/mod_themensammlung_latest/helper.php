<?php

/**
 * @version		$Id: helper.php 63 2011-04-27 01:35:59Z chdemko $
 * @package		Dictionary
 * @subpackage	Module Latest
 * @note		Site
 * @copyright	Copyright (C) 2010-2011 Christophe Demko. All rights reserved.
 * @author		Christophe Demko
 * @link		http://joomlacode.org/gf/project/dictionary/
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

// Get helpers from com_dictionary component
require_once JPATH_SITE . '/components/com_themensammlung/helpers.php';

// import the Joomla! arrayhelper library
jimport('joomla.utilities.arrayhelper');

// Get the models of the com_dictionary component
JModelLegacy::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_themensammlung/models');

/**
 * Helper class for mod_dictionary_latest module
 *
 * @since	0.0.1
 */
abstract class modThemensammlungLatestHelper
{
	/**
	 * Function to get the list of items
	 *
	 * @params	JRegistry	Module parameters
	 *
	 * @return	 array	List of items
	 *
	 * @since	0.0.1
	 */
	public static function getList($params) 
	{

		// Get the application
		$app = JFactory::getApplication();

		// Get an instance of the Entries model
		$model = JModelLegacy::getInstance('Entries', 'ThemensammlungModel', array('ignore_request' => true));

		// State filter
		$model->setState('filter.state', array(1));
		$model->setState('filter.published', array(1));
		$model->setState('filter.publish_down', array(1));

		// Access filter
                $access = !JComponentHelper::getParams('com_themensammlung')->get('show_noauth');
		$authorised = JFactory::getUser()->getAuthorisedViewLevels();
		$model->setState('filter.access', $authorised);

		// Category filter
		if ($catids = $params->get('catid')) 
		{
			foreach ($catids as $i => $catid) 
			{
				if ($catid == - 1) 
				{
					$catids[$i] = ThemensammlungSiteHelper::getCurrentCategory();
				}
			}
			JArrayHelper::toInteger($catids);
			//$model->setState('filter.category_id', $catids);
		}

		// Filter by language
		if ($language = $params->get('language')) 
		{
			$model->setState('filter.language', array($language));
		}
		elseif ($app->getLanguageFilter()) 
		{
			$model->setState('filter.language', array(JFactory::getLanguage()->getTag()));
		}

		// Set list parameters
                $ordering=$params->get('ordering', 'a.modified');
                if($ordering=="c_dsc")
                    $ordering='a.created';
                else
                    $ordering='a.modified';
		$model->setState('list.ordering', $ordering);
		$model->setState('list.direction', 'desc');
		$model->setState('list.start', 0);
		$model->setState('list.limit', (int)$params->get('count', 5));

		// Get the items
                $items = $model->getItems();

		foreach ($items as &$item) {

			$item->slug = $item->id.':'.$item->alias;
			$item->catslug = $item->catid;//.':'.$item->category_alias;

			if ($access || in_array($item->access, $authorised))
			{
				// We know that user has the privilege to view the article
				$item->link = JRoute::_('index.php?option=com_themensammlung&view=item&id='.$item->slug.'&catid='.$item->catid.'&Itemid='.$params->get('my_itemid'));
			}
			else {
				$item->link = JRoute::_('index.php?option=com_user&view=login');
			}

			//$item->introtext = JHtml::_('content.prepare', $item->introtext);
		}

		return $items;
		//return $model->getItems();
                
	}
}

