<?php

/**
 * @version		$Id: default_body.php 63 2011-04-27 01:35:59Z bfoecke $
 * @package		Themensammlung
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Benjamin F�cke. All rights reserved.
 * @author		Benjamin F�cke
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
error_reporting(E_ALL);
// Get the view levels
$user = JFactory::getUser();
$access = $user->getAuthorisedViewLevels();
$firstletters = array();
$blankline = false;
$parsecharTmp=0;
?>
<?php foreach($this->items as $i => $item) : ?>
<tr class="<?php echo ($item->published == 0) ? 'system-unpublished' : '';?> cat-list-row<?php echo $i % 2; ?>">
	
        
        
        <td valign="top">

            <?php
                
                $parsechar=mb_substr($item->title,0,1,"utf-8");
                if (ord($parsechar)==195){
                    if($parsecharTmp>0)
                        $parsechar="a";
                    if($parsecharTmp>65)
                        $parsechar="o";
                    if($parsecharTmp>79)
                        $parsechar="u";
                }
                
                $parsechar = strtoupper($parsechar);
            ?>
            <?php if (!in_array($parsechar, $firstletters)) : ?>
                <?php if (ord($parsechar)!==195)
                        $parsecharTmp=ord($parsechar);
                    ?>
                <?php $firstletters[] = $parsechar; ?>
                <?php $blankline = true; ?>
            <?php endif; ?>
                 <span style='font-weight:bold;'><a href="<?php echo JRoute::_(ThemensammlungHelperRoute::getEntryRoute($item->slug, $item->catid)); ?>"><?php echo $item->title ?></span>

        </td>
	<td align="right">
		<?php echo $item->hits; ?>
	</td>
        <td>
		<?php echo implode(" ",$item->tags); ?>
	</td>
</tr>
<?php //endif; ?>
<?php endforeach; ?>

