<?php

/**
 * @version		$Id: default_items.php 63 2011-04-27 01:35:59Z bfoecke $
 * @package		Themensammlung
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Benjamin F�cke. All rights reserved.
 * @author		Benjamin F�cke
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

JHtmlBehavior::framework();

$params	= $this->params;
$prefix = JRequest::getVar('prefix', $params->get('prefix',''));
$dlang = JRequest::getVar('dlang', $params->get('dlang',''));
?>

<table cellpadding="0" cellspacing="0" border="0" class="display" id="themensammlung_items">
    <?php if ($params->get('show_headings', 1)) : ?>
        <thead><?php echo $this->loadTemplate('head');?></thead>
    <?php endif; ?>
        <tbody><?php echo $this->loadTemplate('body');?></tbody>
    <?php if ($params->get('show_pagination', 1)) : ?>
        <tfoot><?php echo $this->loadTemplate('foot');?></tfoot>
    <?php endif; ?>
</table>


