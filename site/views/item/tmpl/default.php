<?php
/**
 * @version		$Id: default.php 63 2011-04-27 01:35:59Z bfoecke $
 * @package		Themensammlung
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Benjamin F�cke. All rights reserved.
 * @author		Benjamin F�cke
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 * ERWEITERT: 	E-Mail / Drucksymbol in Bootstrap 3 Icons ge�ndert
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

// Get access view levels
$user = JFactory::getUser();
$access = $user->getAuthorisedViewLevels();

// Get current menu item
$menu = JFactory::getApplication()->getMenu();
$active = $menu->getActive();
if ($active) {
    $itemId = $active->id;
} else {
    $itemId = 0;
}

// Create shortcuts to parameters.
$params = $this->params;

// Compute selected asset permissions.
$user = JFactory::getUser();
$userId = $user->get('id');
$asset = 'com_themensammlung.entry.' . $this->item->id;

// Compute some flags
$showPrint = $params->get('show_print_icon', 1);
$showEmail = $params->get('show_email_icon', 1);
// $showEdit		= $user->authorise('core.edit', $asset) || !empty($userId) && $user->authorise('core.edit.own', $asset) && $userId == $this->item->created_by;
$showCategory = $params->get('show_category', 1) && $this->item->catid;
$showLanguage = $params->get('show_language', 1);
$showCreate = $params->get('show_create_date', 1);
$showModify = $params->get('show_modify_date', 1);
$showHits = $params->get('show_hits', 1);
$showVariants = $params->get('show_variants', 1) && (count($this->item->tags) > 1 || (isset($this->item->tags[0]) && $this->item->tags[0] != $this->item->title));
$showPdfs = $params->get('show_pdfs', 1) && (count($this->item->pdfs) >= 1 );
$showLinks = $params->get('show_links', 1) && (count($this->item->links) >= 1 );
$entryID = $this->item->id;
$showComments = $params->get('show_comments', 1) && (count($this->item->text2) >= 1 );
?>
<div id="themensammlung" class="themensammlung-entry<?php echo $params->get('pageclass_sfx') ?>">
<?php $contactDataArray = ThemensammlungHelperRoute::getContactData($entryID); ?>

<?php if ($params->get('show_page_heading', 1)):
        if ($this->escape($params->get('page_heading'))!="Fachbeitr&amp;auml;ge"):
    ?>
        <h1><?php echo $this->escape($params->get('page_heading')); ?></h1>
<?php endif; ?>        
<?php endif; ?> 
        
    <?php if ($contactDataArray['id']): ?>
        <?php if ($contactDataArray['image']): ?>
            <div class="fachbeitrag_container" style="width:100%; height:100px">
                <div style="float:right" class="fachbeitrag_autor"><a href="<?php echo JRoute::_('index.php?option=com_contact&view=contact&id='.$contactDataArray['id']); ?>"><img align="right" src='<?php echo JURI::base()?><?php echo  $contactDataArray['image'] ?>' title='<?php echo  $contactDataArray['name'] ?>' alt=''/></a><a href="<?php echo JRoute::_('index.php?option=com_contact&view=contact&id='.$contactDataArray['id']); ?>"><?php echo $contactDataArray['name']; ?></a></div>
            </div>
        <?php else: ?>
            <div style="width:100%; height:50px">
                <div style="float:right"><p><a href="<?php echo JRoute::_('index.php?option=com_contact&view=contact&id='.$contactDataArray['id']); ?>"><?php echo $contactDataArray['name']; ?></a></p></div>
            </div>
        <?php endif; ?>
    <?php endif; ?>

    <?php if ($params->get('show_title', 1)): ?>
        <h1 style="margin-top:20px; line-height: 1.3em; text-align: left"><?php echo $this->escape($this->item->title); ?></h1>
        <?php if (isset($this->item->subtitle) && !empty($this->item->subtitle)) : ?>
            <h2 style="text-align: left"><?php echo $this->escape($this->item->subtitle); ?></h2>
        <?php endif; ?>
    <?php endif; ?>

    <?php if (isset($this->item->toc)) : ?>
            <?php echo $this->item->toc; ?>
<?php endif; ?>

    <?php if (in_array($this->item->access, $access)): ?>
        <?php echo $this->item->text; ?>
    <?php else: ?>
        <?php echo JHtml::_('string.truncate', $this->item->text, $params->get('readmore_limit', 100)); ?>
        <p class="readmore">
        <?php if ($user->id == 0): ?>
                <a href="<?php echo JRoute::_('index.php?option=com_users&view=login&Itemid=' . $itemId . '&return=' . base64_encode(JRoute::_(ThemensammlungHelperRoute::getEntryRoute($this->item->id, $this->item->catid)))); ?>"><?php echo JText::_('COM_THEMENSAMMLUNG_REGISTER_TO_READ_MORE'); ?></a>
        <?php endif; ?>
        </p>
    <?php endif; ?>
        
    
        
<?php echo $this->item->event->afterDisplayContent; ?>
        
        <?php if ($showCategory || $showCreate || $showModify || $showHit || $showVariants || $showLanguage) : ?>
        <br /><dl class="entry-info">
            <dt class="entry-info-term"><?php //echo JText::_('COM_THEMENSAMMLUNG_ENTRY_INFO');  ?></dt>

        <?php if ($showCategory): ?>
                <dd class="category-name">
            <?php echo JText::_('COM_THEMENSAMMLUNG_ENTRY_CATEGORY'); ?>
        <?php if ($params->get('link_category', 1) && ($params->get('show_noauth', 0) || in_array($this->item->category->access, $access))): ?>
                        <a href="<?php echo JRoute::_(ThemensammlungHelperRoute::getCategoryRoute($this->item->category->slug)); ?>"><?php echo $this->escape($this->item->category->title); ?></a>
                <?php else: ?>
                    <?php echo $this->escape($this->item->category->title); ?>
                    <?php endif; ?>
                </dd>
                <?php endif; ?>

                <?php if ($showLanguage) : ?>
                <dd class="language">
                    <?php echo JText::_('COM_THEMENSAMMLUNG_ENTRY_LANGUAGE'); ?>
                <?php echo $this->escape($this->item->language_title); ?>
                </dd>
            <?php endif; ?>

                <?php if ($showCreate) : ?>
                <dd class="create"><?php echo JText::sprintf('COM_THEMENSAMMLUNG_ENTRY_CREATED_DATE_ON', JHtml::_('date', $this->item->created, JText::_('DATE_FORMAT_LC2')), $this->item->created_by_alias ? $this->item->created_by_alias : $this->item->author); ?></dd>
                <?php endif; ?>

            <?php if ($showModify) : ?>
                <dd class="modified"><?php echo JText::sprintf('COM_THEMENSAMMLUNG_ENTRY_LAST_UPDATED', JHtml::_('date', $this->item->modified, JText::_('DATE_FORMAT_LC2')), $this->item->created_by_alias ? $this->item->created_by_alias : $this->item->author); ?></dd>
            <?php endif; ?>

            <?php if ($showHits) : ?>
                <dd class="hits"><?php echo JText::sprintf('COM_THEMENSAMMLUNG_ENTRY_HITS', $this->item->hits); ?></dd>
            <?php endif; ?>

        </dl>
     <?php endif; ?>
        
        <?php if ($showPrint || $showEmail) : ?>
        <p class="actions pull-right" style="margin-top: 0px;">
            <?php if ($this->print) : ?>
                <?php echo str_replace( '<img src="/media/system/images/printButton.png" alt="Drucken" />', '<span class="glyphicon glyphicon-print print-icon"></span>', JHtml::_('icon.printScreen', $this->item, $params) ); ?>
        <?php else : ?>
            <?php if ($showPrint) : ?>
                    <?php echo str_replace( '<img src="/media/system/images/printButton.png" alt="Drucken" />', '<span class="glyphicon glyphicon-print print-icon"></span>', JHtml::_('icon.printPopup', $this->item, $params) ); ?>
                <?php endif; ?>

                <?php if ($showEmail) : ?>
                    <?php echo str_replace( '<img src="/media/system/images/emailButton.png" alt="E-Mail" />', '<span class="glyphicon glyphicon-envelope email-icon"></span>', JHtml::_('icon.email', $this->item, $params) ); ?>
                <?php endif; ?>

            <?php endif; ?>
        </p>
        <?php endif; ?>
    <hr>

        <?php if ($showComments): ?>
        <div id="jsapp-files" class="app-box">
            <div class="app-box-header">
                <div class="app-box-header">
                    <h2 class="app-box-title"><?php echo JText::_('COM_THEMENSAMMLUNG_COMMENTS'); ?></h2>
                </div>
            </div>
            <div class="app-box-content">
                <ul>
    <?php foreach ($this->item->text2 as $key => $comment): ?>
                        <li><a href="index.php?option=com_content&view=article&id=<?php echo  substr($comment, 0, strpos($comment, '-')) ?>" target="_blank"><?php echo substr(strstr($comment, '-'), 1); ?></a><br /></li>
    <?php endforeach ?>
                </ul>
            </div>
        </div>
                <?php endif; ?>

                <?php if ($showPdfs): ?>
        <div id="jsapp-files" class="app-box">
            <div class="app-box-header">
                <div class="app-box-header">
                    <h2 class="app-box-title"><?php echo JText::_('COM_THEMENSAMMLUNG_PDFS'); ?></h2>
                </div>
            </div>
            <div class="app-box-content">		
                <ul style="list-style-image: url('images/pdf_16x16.png');">
    <?php foreach ($this->item->pdfs as $key => $pdf): ?>
                        <li><a href="pdf_show.php?id=<?php echo  $pdf->id ?>"><?php echo  $pdf->filename ?></a><br /></li>
    <?php endforeach ?>
                </ul>		
            </div>
        </div>
                <?php endif; ?>

                <?php if ($showLinks): ?>        
        <div id="jsapp-links" class="app-box">
            <div class="app-box-header">
                <div class="app-box-header">
                    <h2 class="app-box-title"><?php echo JText::_('COM_THEMENSAMMLUNG_LINKS'); ?></h2>
                </div>
            </div>
            <div class="app-box-content">		
                <ul style="list-style-image: url('images/folder_link_16x16.png');">
    <?php foreach ($this->item->links as $key => $link): ?>
                        <li><a href="<?php echo  $link->link ?>" target="_blank"><?php echo  $link->description ?></a><br /></li>
    <?php endforeach ?>
                </ul>		
            </div>
        </div>
                <?php endif; ?>

                <?php if ($showVariants): ?>
        <div id="jsapp-tags" class="app-box">
            <div class="app-box-header">
                <div class="app-box-header">
                    <h2 class="app-box-title"><?php echo JText::_('COM_THEMENSAMMLUNG_TAGS'); ?></h2>
                </div>
            </div>
            <div class="app-box-content">		
                <ul>
    <?php foreach ($this->item->tags as $key => $tag): ?>
                        <li><a href="<?php echo JURI::base()?>fachbeitraege/beitraege-von-a-z?searchword=<?php echo $tag ?>"><?php echo $tag; ?></a><br /></li>
    <?php endforeach ?>
                </ul>		
            </div>
        </div>
    <?php endif; ?>
</div>

