<?php

/**
 * @version		$Id: default.php 36 2011-03-30 15:24:58Z bfoecke $
 * @package		Themensammlung
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Benjamin F�cke. All rights reserved.
 * @author		Benjamin F�cke
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// Create shortcuts to parameters.
$params			= $this->params;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');

                // Get the PathWay object from the application
		$app		= JFactory::getApplication();
		$pathway	= $app->getPathway();
		$items		= $pathway->getPathWay();

		$count = count($items);
                
		for ($i = 0; $i < $count; $i ++)
		{
			$items[$i]->name = stripslashes(htmlspecialchars($items[$i]->name, ENT_COMPAT, 'UTF-8'));
			$items[$i]->link = JRoute::_($items[$i]->link);
		}

$showPageHeading = $params->def('show_page_heading', 1);

if (isset($_GET['searchword']))
    $searchstring='"oSearch": {"sSearch": "'.$_GET['searchword'].'"},';
?>


<script type="text/javascript">
    
    var asInitVals = new Array();
    jQuery(document).ready( function() {
				
				var oTable = jQuery('#themensammlung_items').dataTable( {
                                        "bJQueryUI": true,
                                        "oLanguage": {
                                            "sLengthMenu": "Zeige _MENU_ Eintr&auml;ge pro Seite",
                                            "sZeroRecords": "Keine Treffer",
                                            "sInfo": "Zeige _START_ bis _END_ von _TOTAL_ Eintr&auml;gen",
                                            "sInfoEmpty": "Zeige 0 bis 0 von 0 Eintr&auml;gen",
                                            "sInfoFiltered": "(gefiltert von _MAX_ Gesamteintr&auml;gen)",
                                            "sSearch": "Suche"
                                        },
                                        <?php echo $searchstring; ?>
                                        "bPaginate": false,
                                        "bLengthChange": false,
                                        "bInfo": false,
                                        "bScrollCollapse": true,
                                        "bSort": false,
                                        "aoColumnDefs": [
                                            { "bVisible": false, "aTargets": [ 2 ] },
                                            { "bSearchable": true, "aTargets": [0,1,2] }
                                        ]
				} );   
			} );
</script>


<div class="themensammlung-alphabetical<?php echo $params->get('pageclass_sfx');?>">
<?php if ($showPageHeading) : ?>
	<h1><?php echo $this->escape($params->get('page_heading')); ?></h1>
<?php endif; ?>

	<div class="alphabetical-items">

		<!--<h3><?php echo JText::_('COM_THEMENSAMMLUNG_CATEGORY_ENTRIES') ; ?></h3>-->
		<?php echo $this->loadTemplate('items'); ?>

	</div>
</div>

