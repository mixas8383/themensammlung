<?php

/**
 * @version		$Id: view.html.php 63 2011-04-27 01:35:59Z chdemko $
 * @package		Themensammlung
 * @subpackage	Component
 * @copyright	Copyright (C) 2011 Hofmann Büroorganisation. All rights reserved.
 * @author		Tilo Ziegler
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

/**
 * Alphabetical View of Themensammlung component
 */
class ThemensammlungViewAuthors extends JViewLegacy
{
	/**
	 * Execute and display a layout script.
	 *
	 * @param	string $tpl	The name of the layout file to parse.
	 *
	 * @return	void|JError
	 *
	 * @since	0.0.1
	 *
	 * @see		JView::display
	 */
	public function display($tpl = null) 
	{
		$this->params = $this->get('Params');
                $this->get('State')->set('list.limit', 1000);
		JRequest::setVar('prefix', JRequest::getVar('prefix', $this->params->get('prefix')));
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');
		$this->state = $this->get('State');
		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
			return JError::raiseWarning(500, implode('<br />', $errors));
		}

		// Prepare the document
		$this->prepareDocument();
		parent::display($tpl);
	}

	/**
	 * Method to set up the document properties
	 *
	 * @since	0.0.1
	 *
	 * @return void
	 */
	protected function prepareDocument() 
	{
		$app = JFactory::getApplication();
		$document = JFactory::getDocument();

		// Because the application sets a default page title,
		// we need to get it from the menu item itself

		$menu = $app->getMenu()->getActive();
		if ($menu) 
		{
			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
		}
		else
		{
			$this->params->def('page_heading', JText::_('COM_THEMENSAMMLUNG_DEFAULT_PAGE_TITLE'));
		}

		// Set the page title
		$title = $this->params->get('page_title', '');
		if (empty($title)) 
		{
			$title = $app->getCfg('sitename');
		}
		elseif ($app->getCfg('sitename_pagetitles', 0)) 
		{
			$title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
		}
		$document->setTitle($title);

		// Add robots info
		if ($this->params->get('robots')) 
		{
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}
	}
}
