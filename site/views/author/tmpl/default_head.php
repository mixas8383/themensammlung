<?php

/**
 * @version		$Id: default_head.php 36 2011-03-30 15:24:58Z chdemko $
 * @package		Themensammlung
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Christophe Demko. All rights reserved.
 * @author		Christophe Demko
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
?>
<tr>
	<th>
		<?php echo JText::_('Fachbeitr&auml;ge');?>
	</th>
	<th width="5%" nowrap="nowrap">
		<?php echo JText::_('JGLOBAL_HITS');?>
	</th>
        <th>
		Schlagworte
	</th>
</tr>

