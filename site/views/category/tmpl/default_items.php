<?php

/**
 * @version		$Id: default_items.php 20 2011-03-28 23:33:03Z chdemko $
 * @package		Themensammlung
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Christophe Demko. All rights reserved.
 * @author		Christophe Demko
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

JHtmlBehavior::framework();

$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));
$params		= $this->params;
?>
<form action="<?php echo JFilterOutput::ampReplace(JFactory::getURI()->toString()); ?>" method="post" name="adminForm" id="adminForm">
	<table class="category">
<?php if ($params->get('show_headings', 1)) : ?>
		<thead><?php echo $this->loadTemplate('head');?></thead>
<?php endif; ?>
		<tbody><?php echo $this->loadTemplate('body');?></tbody>
<?php if ($params->get('show_pagination', 1)) : ?>
		<tfoot><?php echo $this->loadTemplate('foot');?></tfoot>
<?php endif; ?>
	</table>
	<div>
		<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
	</div>
</form>

