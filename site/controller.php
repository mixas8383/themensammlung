<?php

/**
 * @version		$Id: controller.php 63 2011-04-27 01:35:59Z bfoecke $
 * @package		Themensammlung
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Benjamin F�cke, Inc. All rights reserved.
 * @author		Benjamin F�cke
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controller library
jimport('joomla.application.component.controller');

/**
 * General Controller of Themensammlung component
 *
 * @since	0.0.1
 */
class ThemensammlungController extends JControllerLegacy
{
	/**
	 * @var		string	The default view for the display method.
	 *
	 * @since	0.0.1
	 *
	 * @see		JController::$default_view
	 */
	protected $default_view = 'alphabetical';
}
