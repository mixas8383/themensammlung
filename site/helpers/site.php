<?php

/**
 * @version		$Id: route.php 42 2011-03-31 09:12:23Z bfoecke $
 * @package		Themensammlung
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Benjamin F�cke. All rights reserved.
 * @author		Benjamin F�cke
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Themensammlung Component Helper
 *
 * @since	0.0.2
 */
abstract class ThemensammlungSiteHelper
{
	/**
	 * @var	string			The current component
	 *
	 * @since	0.0.2
	 */
	protected static $option;

	/**
	 * @var	string			The current view
	 *
	 * @since	0.0.2
	 */
	protected static $view;

	/**
	 * Get the current component
	 *
	 * @return	string		The current component
	 *
	 * @since	0.0.2
	 */
	protected static function getOption() 
	{
		if (!isset(self::$option)) 
		{
			self::$option = JRequest::getCmd('option');
		}
		return self::$option;
	}

	/**
	 * Get the current view
	 *
	 * @return	string		The current view
	 *
	 * @since	0.0.2
	 */
	protected static function getview() 
	{
		if (!isset(self::$view)) 
		{
			self::$view = JRequest::getCmd('view');
		}
		return self::$view;
	}

	/**
	 * Get the current viewed category of the Themensammlung component
	 *
	 * @return	int			The current viewed category
	 *
	 * @since	0.0.2
	 */
	public static function getCurrentCategory() 
	{
		// Current component is com_themensammlung
		if (self::getOption() == 'com_themensammlung') 
		{
			$view = self::getView();

			// Current view is the item view
			if ($view == 'item') 
			{
				return JRequest::getInt('catid', 0);
			}

			// Current view is the category view
			elseif ($view == 'category') 
			{
				return JRequest::getInt('id', 0);
			}

			// Finally fallback to null
			else 
			{
				return null;
			}
		}

		// Fallback to null
		else 
		{
			return null;
		}
	}

	/**
	 * Get the current viewed item of the Themensammlung component
	 *
	 * @return	int|null	The current viewed item
	 *
	 * @since	0.0.2
	 */
	public static function getCurrentItem() 
	{
		// Current component is com_themensammlung
		if (self::getOption() == 'com_themensammlung') 
		{
			// Current view is the item view
			if (self::getView() == 'item') 
			{
				return JRequest::getInt('id', 0);
			}

			// Finally return null
			else 
			{
				return null;
			}
		}

		// Finally return null
		else 
		{
			return null;
		}
	}

	/**
	 * Get the current prefix of the Themensammlung component
	 *
	 * @return	string|null	The current prefix
	 *
	 * @since	0.0.2
	 */
	public static function getCurrentPrefix() 
	{
		// Current component is com_themensammlung
		if (self::getOption() == 'com_themensammlung') 
		{
			// Current view is the item view
			if (self::getView() == 'alphabetical') 
			{
				return JRequest::getString('prefix', '');
			}
                        elseif (self::getView() == 'lastmodified') 
			{
				return JRequest::getString('prefix', '');
			}
			// Finally return null
			else 
			{
				return null;
			}
		}

		// Finally return null
		else 
		{
			return null;
		}
	}

	/**
	 * Get the current language of the Themensammlung component
	 *
	 * @return	string|null	The current language
	 *
	 * @since	0.0.2
	 */
	public static function getCurrentLanguage() 
	{
		// Current component is com_themensammlung
		if (self::getOption() == 'com_themensammlung') 
		{
			// Current view is the item view
			if (self::getView() == 'alphabetical') 
			{
				return JRequest::getString('dlang', '');
			}
                        elseif (self::getView() == 'lastmodified') 
			{
				return JRequest::getString('dlang', '');
			}
			// Finally return null
			else 
			{
				return null;
			}
		}

		// Finally return null
		else 
		{
			return null;
		}
	}
}
