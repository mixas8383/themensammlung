<?php

/**
 * @version		$Id: themensammlung.php 63 2011-04-27 01:35:59Z bfoecke $
 * @package		Themensammlung
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Benjamin F�cke. All rights reserved.
 * @author		Benjamin F�cke
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import helper files
require_once dirname(__FILE__) . '/helpers.php';

// import joomla controller library
jimport('joomla.application.component.controller');

// Get an instance of the controller prefixed by Themensammlung
$controller = JControllerLegacy::getInstance('Themensammlung');

// Perform the Request task
$controller->execute(JRequest::getCmd('task'));

// Redirect if set by the controller
$controller->redirect();
//
//if(!$controller->redirect()){
//
//    $_REQUEST['view'] = 'item';
//    $_REQUEST['id'] = 2;
//    
//    // Get an instance of the controller prefixed by Themensammlung
//    $controller = JController::getInstance('Themensammlung');
//
//    // Perform the Request task
//    $controller->execute(JRequest::getCmd('task'));
//    $controller->redirect();
//}

