<?php

/**
 * @version		$Id: helpers.php 63 2011-04-27 01:35:59Z bfoecke $
 * @package		Themensammlung
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Benjamin F�cke. All rights reserved.
 * @author		Benjamin F�cke
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// Register all helper classes
JLoader::register('ThemensammlungHelperRoute', dirname(__FILE__) . '/helpers/route.php');
JLoader::register('ThemensammlungCategories', dirname(__FILE__) . '/helpers/category.php');
JLoader::register('ThemensammlungsiteHelper', dirname(__FILE__) . '/helpers/site.php');
