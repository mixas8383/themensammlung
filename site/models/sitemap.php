<?php

/**
 * @version		$Id: sitemap.php 63 2011-04-27 01:35:59Z chdemko $
 * @package		Themensammlung
 * @subpackage          Component
 * @copyright           Copyright (C) 2010-2011 Christophe Demko. All rights reserved.
 * @author		Christophe Demko
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Entry Model from administrator
JLoader::register('ThemensammlungModelEntries', JPATH_ADMINISTRATOR . '/components/com_themensammlung/models/entries.php');

// import the Joomla categories library
jimport('joomla.application.categories');

/**
 * Sitemap Model of Themensammlung component
 *
 * @since	0.0.1
 */
class ThemensammlungModelSitemap extends ThemensammlungModelEntries{

        /**
	 * Method to get category data for the current category
	 *
	 * @return	object
	 *
	 * @since	0.0.1
	 */
	public function getCategory()
	{
		if (!isset($this->category))
		{
			$catids = $this->getState('filter.category_id', array('root'));
			$categories = JCategories::getInstance('Themensammlung', array('access' => false));
			$this->category = $categories->get($catids[0]);
		}
		return $this->category;
	}

        /**
	 * Method to get the current application parameters
	 *
	 * @return	JRegistry	The application parameters
	 *
	 * @since	0.0.1
	 */
	public function getParams()
	{
		if (!isset($this->params))
		{
			$this->params = JFactory::getApplication()->getParams();
			if (!isset($this->category))
			{
				$this->getCategory();
			}
			if ($this->category)
			{
				$this->params->merge($this->category->getParams());
			}
		}
		return $this->params;
	}

        

}

?>