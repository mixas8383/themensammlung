<?php

/**
 * @version		$Id: category.php 63 2011-04-27 01:35:59Z chdemko $
 * @package		Themensammlung
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Christophe Demko. All rights reserved.
 * @author		Christophe Demko
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Entry Model from administrator
JLoader::register('ThemensammlungModelEntries', JPATH_ADMINISTRATOR . '/components/com_themensammlung/models/entries.php');

// import the Joomla categories library
jimport('joomla.application.categories');

/**
 * Category Model of Themensammlung component
 *
 * @since	0.0.1
 */
class ThemensammlungModelCategory extends ThemensammlungModelEntries
{
	/**
	 * @var		array	The authorized ordering fields.
	 *
	 * @since	0.0.1
	 *
	 * @see		JModelList::$filter_fields
	 */
	protected $filter_fields = array('title', 'hits', 'ordering');

	/**
	 * @var		object	The category object.
	 *
	 * @since	0.0.1
	 */
	protected $category;

	/**
	 * @var		JRegistry	The application parameters.
	 *
	 * @since	0.0.1
	 */
	protected $params;

	/**
	 * Method to auto-populate the model state.
	 *
	 * @param	string	$ordering	An optional ordering field.
	 * @param	string	$direction	An optional direction (asc|desc).
	 *
	 * @return	void
	 *
	 * @since	0.0.1
	 *
	 * @see		JModelList::populateState
	 */
	protected function populateState($ordering = null, $direction = null) 
	{
		parent::populateState($ordering, $direction);

		// Get the application
		$app = JFactory::getApplication();

		// Set the category id
		$id = JRequest::getInt('id', 0);
		$this->setState('filter.category_id', array($id));

		// Set the language
		if ($app->getLanguageFilter()) 
		{
			$this->setState('filter.language', array(JFactory::getLanguage()->getTag()));
		}

		// Set the published state
		$this->setState('filter.published', 1);
	}

	/**
	 * Method to get category data for the current category
	 *
	 * @return	object
	 *
	 * @since	0.0.1
	 */
	public function getCategory() 
	{
		if (!isset($this->category)) 
		{
			$catids = $this->getState('filter.category_id', array('root'));
			$categories = JCategories::getInstance('Themensammlung', array('access' => false));
			$this->category = $categories->get($catids[0]);
		}
		return $this->category;
	}

	/**
	 * Method to get a JDatabaseQuery object for retrieving the data set from a database.
	 *
	 * @return	object	A JDatabaseQuery object to retrieve the data set.
	 *
	 * @since	0.0.1
	 *
	 * @see		JModelList::getListQuery
	 */
	protected function getListQuery() 
	{
		
		// Get query from parent
		// Create a new query object.
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$user = JFactory::getUser();
		$app = JFactory::getApplication();

		// Select some fields
		$query->select(
			$this->getState(
				'list.select',
				'a.id,a.published,a.title,a.subtitle,a.text,a.alias,a.catid,a.created,a.created_by,a.created_by_alias,a.modified,a.checked_out,a.checked_out_time,'.
			'a.hits,a.language,a.ordering,a.introduction,a.text2,a.publish_down'
			)
		);

		// From the themensammlung_entry table
		$query->from('#__themensammlung_entry as a');
        $current_date = date( "Y-m-j H:i:00", strtotime( 'now' ) );
        $query->where( '(' . $db->quoteName('a.publish_down') . ' >= ' . $db->quote( $current_date ) . ')'
            . ' OR (' .  $db->quoteName('a.publish_down') . ' = ' . $db->quote( '0000-00-00 00:00:00' ) . ')' );

                // Join over the alternative categories.
		$query->select('acat.alternativecatid')
                        ->join('LEFT', '#__themensammlung_entry_categories AS acat ON acat.eid = a.id');    
                
                // Join over the language
		//$query->select('l.title AS language_title')
		//	->join('LEFT', $db->quoteName('#__languages') . ' AS l ON l.lang_code = a.language');
                
                // Join over the view levels.
                $query->select('ag.title AS access_level');
                    $query->join('LEFT', '#__viewlevels AS ag ON ag.id = a.access');
		
                // Join over the categories.
		//$query->select('c.title AS category_title')
		//	->join('LEFT', '#__categories AS c ON c.id = a.catid');
                
                // Join over the users for the author.
		//$query->select('ua.name AS author_name');
                //$query->select('ua.email AS author_email');
                //$query->join('LEFT', '#__users AS ua ON ua.id = a.created_by');

                // Join over the users for the checked out user.
		//$query->select('uc.name AS editor')
		//	->join('LEFT', '#__users AS uc ON uc.id=a.checked_out');

		            		
                // Join over the associations.
		/*if (JLanguageAssociations::isEnabled())
		{
			$query->select('COUNT(asso2.id)>1 as association')
				->join('LEFT', '#__associations AS asso ON asso.id = a.id AND asso.context=' . $db->quote('com_themensammlung.item'))
				->join('LEFT', '#__associations AS asso2 ON asso2.key = asso.key')
				->group('a.id');
		}*/

		// Filter by access level.
		if ($access = $this->getState('filter.access'))
		{
			$query->where('a.access = ' . (int) $access);
		}

		// Implement View Level Access
		if (!$user->authorise('core.admin'))
		{
			$groups = implode(',', $user->getAuthorisedViewLevels());
			$query->where('a.access IN (' . $groups . ')');
		}

		// Filter by published state
		$published = $this->getState('filter.published');

		if (is_numeric($published))
		{
			$query->where('a.published = ' . (int) $published);
		}
		else if ($published=="*")
		{
			$query->where('a.published IN (-2,0,1,2)');
		}
                else
		{
			$query->where('(a.published = 1 OR a.published = 0)');
		}
                
                // Filter by a single or group of categories.
		$baselevel = 1;
		$categoryId = $this->getState('filter.category_id');

		if (is_numeric($categoryId))
		{
			$cat_tbl = JTable::getInstance('Category', 'JTable');
			$cat_tbl->load($categoryId);
			$rgt = $cat_tbl->rgt;
			$lft = $cat_tbl->lft;
			$baselevel = (int) $cat_tbl->level;
			$query->where('c.lft >= ' . (int) $lft)
				->where('c.rgt <= ' . (int) $rgt);
		}
		elseif (is_array($categoryId))
		{
			JArrayHelper::toInteger($categoryId);
			$categoryId = implode(',', $categoryId);
			$query->where('a.catid IN (' . $categoryId . ') OR acat.alternativecatid IN (' . $categoryId . ')');
		}

                // Filter on the level.
		if ($level = $this->getState('filter.level'))
		{
			$query->where('c.level <= ' . ((int) $level + (int) $baselevel - 1));
		}

		// Filter by author
		$authorId = $this->getState('filter.author_id');

		if (is_numeric($authorId))
		{
			$type = $this->getState('filter.author_id.include', true) ? '= ' : '<>';
			$query->where('a.created_by ' . $type . (int) $authorId);
		}

		// Filter by search in title.
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			elseif (stripos($search, 'author:') === 0)
			{
				$search = $db->quote('%' . $db->escape(substr($search, 7), true) . '%');
				$query->where('(ua.name LIKE ' . $search . ' OR ua.username LIKE ' . $search . ')');
			}
			else
			{
				$search = $db->quote('%' . $db->escape($search, true) . '%');
				$query->where('(a.title LIKE ' . $search . ' OR a.alias LIKE ' . $search . ')');
			}
		}

		// Filter on the language.
		if (is_array($language = $this->getState('filter.language'))) 
                { 
                        $query->where('a.language IN ("*","' . implode(',', $language) . '")'); 
                } 
		else if ($language = $this->getState('filter.language'))
		{
			$query->where('a.language = ' . $db->quote($language));
		}

		// Filter by a single tag.
		$tagId = $this->getState('filter.tag');

		if (is_numeric($tagId))
		{
			$query->where($db->quoteName('tagmap.tag_id') . ' = ' . (int) $tagId)
				->join(
					'LEFT', $db->quoteName('#__themensammlungitem_tag_map', 'tagmap')
					. ' ON ' . $db->quoteName('tagmap.themensammlung_item_id') . ' = ' . $db->quoteName('a.id')
					. ' AND ' . $db->quoteName('tagmap.type_alias') . ' = ' . $db->quote('com_themensammlung.entry')
				);
		}

		// Add the list ordering clause.
		
		$query->order($db->escape('a.title asc'));

		// Select slug
		$query->select('CONCAT_WS(":", a.id, a.alias) as slug');
		return $query;
                
	}
        
        /**
	 * Method to get an array of data items.
	 *
	 * @return	mixed	An array of data items on success, false on failure.
	 *
	 * @since	0.0.2
	 *
	 * @see		JModelList::getItems
	 */
	public function getItems() 
	{
		// Set the view levels
		if ($this->getParams()->get('show_noauth', 0)) 
		{
			$this->setState('filter.access', false);
		}
		else
		{
			$this->setState('filter.access', JFactory::getUser()->getAuthorisedViewLevels());
		}
                
		return parent::getItems();
	}

	/**
	 * Method to get the current application parameters
	 *
	 * @return	JRegistry	The application parameters
	 *
	 * @since	0.0.1
	 */
	public function getParams() 
	{
		if (!isset($this->params)) 
		{
			$this->params = JFactory::getApplication()->getParams();
			if (!isset($this->category)) 
			{
				$this->getCategory();
			}
			if ($this->category) 
			{
				$this->params->merge($this->category->getParams());
			}
		}
		return $this->params;
	}

	/**
	 * Get the left sibling (adjacent) categories.
	 *
	 * @return	mixed	An array of categories or false if an error occurs.
	 * @since	1.6
	 */
	function getLeftSibling()
	{
                // Get the database object
                $db = JFactory::getDbo();

                // Get the tags
                $query = $db->getQuery(true);
                $query->select('id');
                $query->from($db->quoteName('#__categories' ));
                $query->where('rgt=' . (int)($this->category->lft - 1));
                $query->where('level=' . (int)($this->category->level));
                $db->setQuery($query);
                $lft_sibling = $db->loadResult();

                if ($db->getErrorNum()) 
                {
                        $this->setError($db->getErrorMsg());
                        return false;
                }
                if(empty($lft_sibling)){
                    return null;
                }
                
                
                $categories = JCategories::getInstance('Themensammlung', array('access' => false));
                return $categories->get($lft_sibling);
	}

	/**
	 * Get the left sibling (adjacent) categories.
	 *
	 * @return	mixed	An array of categories or false if an error occurs.
	 * @since	1.6
	 */
	function getRightSibling()
	{
		// Get the database object
                $db = JFactory::getDbo();

                // Get the tags
                $query = $db->getQuery(true);
                $query->select('id');
                $query->from($db->quoteName('#__categories' ));
                $query->where('lft=' . (int)($this->category->rgt + 1));
                $query->where('level=' . (int)($this->category->level));
                
                $db->setQuery($query);
                $rgt_sibling = $db->loadResult();

                if ($db->getErrorNum()) 
                {
                        $this->setError($db->getErrorMsg());
                        return false;
                }
                if(empty($rgt_sibling)){
                    return null;
                }
                $categories = JCategories::getInstance('Themensammlung', array('access' => false));
                return $categories->get($rgt_sibling);
	}
}
