<?php

/**
 * @version		$Id: item.php 63 2011-04-27 01:35:59Z bfoecke $
 * @package		Themensammlung
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Benjamin F�cke. All rights reserved.
 * @author		Benjamin F�cke
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Entry Model from administrator
JLoader::register('ThemensammlungModelEntry', JPATH_ADMINISTRATOR . '/components/com_themensammlung/models/entry.php');

/**
 * Item Model of Themensammlung component
 *
 * @since	0.0.1
 */
class ThemensammlungModelItem extends ThemensammlungModelEntry
{
	/**
	 * @var	object	 The entry
	 *
	 * @since	0.0.1
	 */
	protected $item;

	/**
	 * @var	JRegistry	The application parameter
	 *
	 * @since	0.0.1
	 */
	protected $params;

	/**
	 * Return the entry
	 *
	 * @return object	an entry
	 *
	 * @since	0.0.1
	 */
	public function getItem($pk = null) 
	{
		if (!isset($this->item)) 
		{
			if ($this->item = parent::getItem($pk)) 
			{
                            $category = JCategories::getInstance('Themensammlung', array('access' => false))->get(104);

				// Compute category
				if ($this->item->catid != 0 && ($category = JCategories::getInstance('Themensammlung', array('access' => false))->get($this->item->catid))) 
				{
					$this->item->category = $category;
				}

				// Compute author
				if ($this->item->created_by_alias) 
				{
					$this->item->author = $this->item->created_by_alias;
				}
				else
				{
					$author = JTable::getInstance('User');
					if ($author->load($this->item->created_by)) 
					{
						$this->item->author = $author->name;
					}
					else
					{
						$this->item->author = JText::_('COM_THEMENSAMMLUNG_SOMEBODY');
					}
				}

				// Compute modifier
				if ($this->item->modified_by) 
				{
					$modifier = JTable::getInstance('User');
					if ($modifier->load($this->item->modified_by)) 
					{
						$this->item->modifier = $modifier->name;
					}
					else
					{
						$this->item->modifier = JText::_('COM_THEMENSAMMLUNG_SOMEBODY');
					}
				}

				// Compute language title
				$languages = JLanguageHelper::getLanguages('lang_code');
                                if(!isset($languages[$this->item->language])){
                                    $this->item->language_title = 'Deutsch';
                                }else{
                                    $this->item->language_title = $languages[$this->item->language]->title;
                                }
			}
		}
		return $this->item;
	}

	/**
	 * Method to increment the hits counter
	 *
	 * @return	boolean	Success or Failure
	 *
	 * @since	0.0.1
	 */
	public function hit() 
	{
		$pk = $this->getState('item.id');
		$table = $this->getTable();
		if (!$table->hit($pk)) 
		{
			$this->setError($table->getError());
			return false;
		}
		return true;
	}

	/**
	 * Method to get the current application parameters
	 *
	 * @return	JRegistry	The application parameters
	 *
	 * @since	0.0.1
	 */
	public function getParams() 
	{
		if (!isset($this->params)) 
		{
			$this->params = JFactory::getApplication()->getParams();
			if (!isset($this->item)) 
			{
				$this->getItem();
			}
			if ($this->item) 
			{
				$this->params->merge($this->item->params);
			}
		}
		return $this->params;
	}
}
