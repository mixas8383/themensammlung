<?php

/**
 * @version		$Id: authors.php 63 2011-04-27 01:35:59Z chdemko $
 * @package		Themensammlung
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Christophe Demko. All rights reserved.
 * @author		Tilo Ziegler
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Entry Model from administrator
JLoader::register('ThemensammlungModelEntries', JPATH_ADMINISTRATOR . '/components/com_themensammlung/models/entries.php');

// import the Joomla categories library
jimport('joomla.application.categories');

/**
 * Authors Model of Themensammlung component
 *
 * @since	0.0.1
 */
class ThemensammlungModelAuthors extends ThemensammlungModelEntries
{
	protected $params;

	/**
	 * Method to auto-populate the model state.
	 *
	 * @param	string	$ordering	An optional ordering field.
	 * @param	string	$direction	An optional direction (asc|desc).
	 *
	 * @return	void
	 *
	 * @since	0.0.1
	 *
	 * @see		JModelList::populateState
	 */
	protected function populateState($ordering = null, $direction = null) 
	{
		parent::populateState($ordering, $direction);

		// Get the application
		$app = JFactory::getApplication();

		// Set the language
		$language = JRequest::getVar('dlang', '');
		if ($language) 
		{
			$this->setState('filter.language', array($language));
		}
		else
		{
			$this->setState('filter.language', array(JFactory::getLanguage()->getTag()));
		}

		// Set the prefix
		$prefix = JRequest::getVar('prefix', '');
		if ($prefix) 
		{
			$this->setState('filter.prefix', array($prefix));
		}
		else
		{
			$this->setState('filter.prefix', true);
		}

		// Set the published state
		$this->setState('filter.published', 1);
                                
	}

	/**
	 * Method to get a JDatabaseQuery object for retrieving the data set from a database.
	 *
	 * @return	object	A JDatabaseQuery object to retrieve the data set.
	 *
	 * @since	0.0.1
	 *
	 * @see		JModelList::getListQuery
	 */
	protected function getListQuery() 
	{
		// Get query from parent
		//$query = parent::getListQuery();

                // Create a new query object.
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
                
		// Select some fields
		$query->select(
			$this->getState(
				'list.select',
				'a.id,a.published,a.title,a.subtitle,a.text,a.alias,a.catid,a.created,a.created_by,a.created_by_alias,a.modified,a.checked_out,a.checked_out_time,'.
			'a.hits,a.language,a.ordering,a.introduction,a.text2,a.publish_down,
                        COUNT(a.id)                     AS number_of_entries,
			CONCAT_WS(":", a.id, a.alias) as slug' 
			)
		);

		// From the themensammlung_entry table
		$query->from('#__themensammlung_entry as a');
        $current_date = date( "Y-m-j H:i:00", strtotime( 'now' ) );
        $query->where( '(' . $db->quoteName('a.publish_down') . ' >= ' . $db->quote( $current_date ) . ')'
            . ' OR (' .  $db->quoteName('a.publish_down') . ' = ' . $db->quote( '0000-00-00 00:00:00' ) . ')' );

                // Join over the contacts/authors
                $query->select('cd.name as contact_name, SUBSTRING_INDEX(cd.name," ",-1) as contact_name_order, cd.id as contact_id');
                $query->join('LEFT', '#__contact_details AS cd ON a.author = cd.id');
                
		// Join over the language
		$query->select('l.title AS language_title')
			->join('LEFT', $db->quoteName('#__languages') . ' AS l ON l.lang_code = a.language');
                
		// Join over the view levels.
                $query->select('ag.title AS access_level');
                    $query->join('LEFT', '#__viewlevels AS ag ON ag.id = a.access');
				
                // Join over the users for the author.
		$query->select('ua.name AS author_name');
                $query->select('ua.email AS author_email');
                $query->join('LEFT', '#__users AS ua ON ua.id = a.created_by');

                // Join over the users for the checked out user.
		$query->select('uc.name AS editor')
			->join('LEFT', '#__users AS uc ON uc.id=a.checked_out');

		// Filter by access level.
		if ($access = $this->getState('filter.access')) 
		{
			$query->where('a.access IN (' . implode(',', $access) . ')');
		}
                
                
                
		// Filter by published state
		$published = $this->getState('filter.published');

		if (is_numeric($published))
		{
			$query->where('a.published = ' . (int) $published);
		}
		else if ($published=="*")
		{
			$query->where('a.published IN (-2,0,1,2)');
		}
                else
		{
			$query->where('(a.published = 1 OR a.published = 0)');
		}

                		
                // Filter by search in title.
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0) 
			{
				$query->where('a.id = ' . (int)substr($search, 3));
			}
			else
			{
				$search = $db->quote('%' . $db->escape($search, true) . '%');
				$query->where('(cd.name LIKE ' . $search . ')');
			}
		}

                
                $query->where('cd.name <> ""');
                
		// Add the list ordering clause.
		$orderCol = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');
                $query->group('cd.id');
		$query->order('contact_name_order');

		if ($orderCol) 
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}
                
		// return query to JModelList
		return $query;
                
		// Select slug
		//$query->select('CONCAT_WS(":", e.id, e.alias) as slug');
		//return $query;
	}

	/**
	 * Method to get an array of data items.
	 *
	 * @return	mixed	An array of data items on success, false on failure.
	 *
	 * @since	0.0.2
	 *
	 * @see		JModelList::getItems
	 */
	public function getItems() 
	{
		// Set the view levels
		if ($this->getParams()->get('show_noauth')) 
		{
			$this->setState('filter.access', false);
		}
		else
		{
			$this->setState('filter.access', JFactory::getUser()->getAuthorisedViewLevels());
		}
		return parent::getItems();
	}

	/**
	 /**
	 * Method to get the current application parameters
	 *
	 * @return	JRegistry	The application parameters
	 *
	 * @since	0.0.1
	 */
	public function getParams() 
	{
		if (!isset($this->params)) 
		{
			$this->params = JFactory::getApplication()->getParams();
		}
		return $this->params;
	}
}
