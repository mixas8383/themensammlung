if("undefined"===typeof Joomla)var Joomla={};
Joomla.removepdf = function(id, url){
    var data = {"id":id}
    var pdftext = document.getElementById('pdftext');
    var listelement = document.getElementById('pdf_'+id);
    var request = new Request.JSON({

        url: url,

        onRequest: function(){
        pdftext.set('text', 'Deleting...');
        pdftext.set('class', '');
        },

        onComplete: function(jsonObj) {
        pdftext.empty();
        if(jsonObj.status == 1){
            pdftext.set('class', 'success');
            listelement.parentNode.removeChild(listelement);
        }else{
            pdftext.set('class', 'error');
        }
        pdftext.set('text', jsonObj.msg);
        },

        data: { 
        json: JSON.encode(data)
        }

    }).send();
}

Joomla.addPdfUploadForm = function(){
    var list = document.getElementById('pdf_upload_list');
    var element = document.getElementById('pdf_upload_list').getElementsByTagName('div')[0].cloneNode(true);
    element.getElementsByTagName('input')[0].value='';
    list.appendChild(element);
}