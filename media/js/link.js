if("undefined"===typeof Joomla)var Joomla={};
Joomla.addlink = function(link, description, url){
    var data = {"link":link, "description":description}
    var linktext = document.getElementById('linktext');
    var inputLink = document.getElementById('link');
    var inputLinkDescription = document.getElementById('link_description');
    var request = new Request.JSON({

      url: url,

      onRequest: function(){
        linktext.set('text', 'Saving...');
      },

      onComplete: function(jsonObj) {
        linktext.empty();
        if(jsonObj.status == 1){
            document.getElementById('linklist').innerHTML = document.getElementById('linklist').innerHTML+jsonObj.entry;
            linktext.set('class', 'success');
            inputLink.value = 'http://';
            inputLinkDescription.value = '';
        }else{
            linktext.set('class', 'error');
        }
        linktext.set('text', jsonObj.msg);
      },

      data: { 
        json: JSON.encode(data)
      }

    }).send();
}


    
Joomla.removelink = function(id, url){
    var data = {"id":id}
    var linktext = document.getElementById('linktext');
    var listelement = document.getElementById('link_'+id);

    var request = new Request.JSON({

      url: url,

      onRequest: function(){
        linktext.set('text', 'Deleting...');
      },

      onComplete: function(jsonObj) {
        linktext.empty();
        if(jsonObj.status == 1){
            linktext.set('class', 'success');
            listelement.parentNode.removeChild(listelement);
        }else{
            linktext.set('class', 'error');
        }
        linktext.set('text', jsonObj.msg);
      },

      data: { 
        json: JSON.encode(data)
      }

    }).send();
}